#include <termios.h>

#ifndef _TABLE_H_
#define _TABLE_H_


typedef struct flaginfo { tcflag_t fl_value; char *fl_name;} flaginfo;
flaginfo ccflags[7];

typedef struct baudrate {speed_t baud; int rate;} baudrate;
baudrate termbaud[19];

typedef struct settermios { tcflag_t fl_value; char *fl_name;
                            char key; tcflag_t *location;} settermios;
settermios termiosflg[10];

void set_ttyinfo(struct termios *ttydata, struct settermios termiosflg[]);

#endif