#include "table.h"
#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

/* table.c  - helper program to implement a table structure
*
*
*      functions are
*			void set_ttyinfo(struct termios *ttydata, struct settermios termiosflg[])
*
*		struct variables which represents tables are
*			struct baudrate - table to capture baud rates
*			struct settermios - table that has all the supported set parameters of sttyl
*			struct flaginfo - table used to display the status of special terminal characters
*/

// variable that holds the baud rate denotes in man termios
baudrate termbaud[19] =
{
    {B0, 0},
    {B50, 50},
    {B75, 75},
    {B110, 110},
    {B134, 134},
    {B200, 200},
    {B150, 150},
    {B300, 300},
    {B600, 600},
    {B1200, 1200},
    {B1800, 1800},
    {B2400, 2400},
    {B4800, 4800},
    {B9600, 9600},
    {B19200, 19200},
    {B38400, 38400},
    {B57600, 57600},
    {B115200, 115200},
    {B230400, 230400}
};

// variable to hold the special charater flags
flaginfo ccflags[7] =
{
    {VINTR, "intr"},
    {VERASE, "erase"},
    {VKILL, "kill"},
    {VSTART, "start"},
    {VSTOP, "stop"},
    {VWERASE, "werase"},
    {0,	NULL}
};

// variable that holds the sttyl supported features
settermios termiosflg[10] =
{
    {ICRNL, "icrnl", 'i', NULL},
    {ECHO, "echo", 'l', NULL},
    {ECHOE, "echoe", 'l', NULL},
    {ICANON, "icanon", 'l', NULL},
    {ISIG, "isig", 'l', NULL},
    {OPOST, "opost", 'o', NULL},
    {HUPCL, "hupcl", 'c', NULL},
    {VERASE, "erase", 's', NULL},
    {VKILL, "kill", 's', NULL},
    {0,	NULL, 'o', NULL}
};

void set_ttyinfo(struct termios *ttydata, struct settermios termiosflg[])
/*
 * set_ttyinfo -- sets the address of c_iflag, c_oflag, c_lflag, c_cflag
                  address to termiosflg struct array
 *  args: *ttydata, termiosflg
 *  rets: none
 */
{
    for (int setflg = 0; termiosflg[setflg].key != 's'; setflg++)
    {
        // logic to set the address to the table termiosflg
        switch (termiosflg[setflg].key)
        {
            case 'i':
            {
                termiosflg[setflg].location = &(ttydata->c_iflag);
                break;
            }
            case 'l':
            {
                termiosflg[setflg].location = &(ttydata->c_lflag);
                break;
            }
            case 'o':
            {
                termiosflg[setflg].location = &(ttydata->c_oflag);
                break;
            }
            case 'c':
            {
                termiosflg[setflg].location = &(ttydata->c_cflag);
                break;
            }
        }
    }
}