#include <stdio.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "table.h"

/* sttyl.c  - main program that implements the function sttyl
*
*
*  functions are
*			int showbaud(int thespeed, struct baudrate termbaud[]);
*			void display_settings(struct termios *current_ttyinfo,
*						struct settermios termiosflg[],
*			                  struct baudrate termbaud[],
*			                  struct flaginfo ccflags[]);
*
*			void get_ttyinfo(struct termios *ttydata);
*			void show_ccset(struct termios *current_ttyinfo,
*			                  	struct flaginfo ccflags[]);
*
*			int processcmdargs(struct termios *current_ttyinfo,
*			                  		struct settermios termiosflg[],
*			                  		int argc, char **argv);
*
*			void set_iocl_settings(char *flag, tcflag_t *location,
*			                  			tcflag_t fl_value);
*
*			void show_flagset(struct settermios termiosflg[]);
 *
 */

// function declaration
int showbaud(int thespeed, struct baudrate termbaud[]);
void display_settings(struct termios *current_ttyinfo,
                        struct settermios termiosflg[],
                        struct baudrate termbaud[],
                        struct flaginfo ccflags[]);

void get_ttyinfo(struct termios *ttydata);
void show_ccset(struct termios *current_ttyinfo,
                        struct flaginfo ccflags[]);

int processcmdargs(struct termios *current_ttyinfo,
                        struct settermios termiosflg[],
                        int argc, char **argv);

void set_iocl_settings(char *flag, tcflag_t *location,
                        tcflag_t fl_value);

void show_flagset(struct settermios termiosflg[]);

// definition of magic numbers

#define BAUD 19
#define ERROR -1
#define CNTRLCHARS 32
#define SPACE 127
#define SUCCESS 0
#define FAILURE 1


int main(int argc, char **argv)
/*
 * main -- main program starts
 *  args: argc, **argv
 *  rets: 1 on FAILURE and 0 of SUCCESS
 */
{
    // variable to hold terminal information
    struct termios current_ttyinfo;

    // obtains the current terminal information
    get_ttyinfo(&current_ttyinfo);

    // sets the address of iocl flag to the termiosflg table defined in table.h
    set_ttyinfo(&current_ttyinfo, termiosflg);

    // logic to that displays/set and clear settings
    if (argc == 1)
    {
        display_settings(&current_ttyinfo, termiosflg, termbaud, ccflags);
    }
    else
    {
        if (processcmdargs(&current_ttyinfo, termiosflg, argc, argv) != FAILURE)
        {
            tcsetattr(0, TCSANOW, &current_ttyinfo);
            return SUCCESS;
        }
    }
    return FAILURE;
}

int processcmdargs(struct termios *current_ttyinfo,
                    struct settermios termiosflg[],
                    int argc, char **argv)
/*
 * processcmdargs -- process the arguments passed to set or clear termios flags
 *  args: current_ttyinfo, termiosflg, argc, argv
 *  rets: 1 on FAILURE and 0 of SUCCESS
 */

{
    // Loops through to process arguments passed
    for (int argcount = 1; argcount < argc; argcount++)
    {
        int found = 0;
        // Loops through and check if the arguments passed are valid
        for (int count = 0; termiosflg[count].fl_value != 0 ; count++)
        {

            // conditions that determines whether to set c_cc or iocl flags
            if
            (
                ( strcmp( argv[argcount], termiosflg[count].fl_name ) == 0 ) ||
                (
                  ( strcmp( &argv[argcount][1], termiosflg[count].fl_name ) == 0 )
                  &&
                  ( termiosflg[count].key != 's' )
                )
            )
            {
                // logic that sets the c_cc flags
                if (termiosflg[count].key == 's')
                {
                    if ((argcount+1 < argc) && (atoi(argv[argcount+1]) >= 0))
                    {
                        current_ttyinfo->c_cc[termiosflg[count].fl_value]
                                            = *argv[argcount+1];
                        argcount=argcount+1;
                    }
                    else
                    {
                        fprintf(stderr, "sttyl not enough arguments:'%s'\n",
                                argv[argcount]);
                        return FAILURE;
                    }
                }
                // logic that sets the iocl flags
                else
                {
                    set_iocl_settings(argv[argcount],
                                        termiosflg[count].location,
                                        termiosflg[count].fl_value );
                }
                found++;
                break;
            }
        }
        if (found == 0)
        {
            fprintf(stderr, "sttyl invalid arguments:'%s'\n", argv[argcount]);
            return FAILURE;
        }
    }
    return SUCCESS;
}

void set_iocl_settings(char *flag, tcflag_t *location, tcflag_t fl_value)
/*
 * set_iocl_settings -- set or clears the iocl terminal flags
 *  args: flag, location, fl_value
 *  rets: none
 */
{
    if (flag[0] != '-') *location |= fl_value;
    else *location &= ~fl_value;
}


void display_settings(struct termios *current_ttyinfo,
                        struct settermios termiosflg[],
                        struct baudrate termbaud[],
                        struct flaginfo ccflags[]
                    )
/*
 * display_settings -- displays the terminal settings
 *  args: current_ttyinfo, termiosflg, termbaud, ccflags
 *  rets: none
 */
{
    struct winsize w;
    // determins the window size
    if (ioctl(0, TIOCGWINSZ, &w) == ERROR)
    {
        perror("Error retrieving terminal size\n");
        exit(1);
    }
    // logic that displays all the current supported settings
    printf("speed %d baud; rows %d; columns %d\n",
                showbaud(cfgetospeed(current_ttyinfo), termbaud),
                    w.ws_row, w.ws_col);
    show_ccset(current_ttyinfo,ccflags);
    show_flagset(termiosflg);
}

void show_ccset(struct termios *current_ttyinfo, struct flaginfo ccflags[])
/*
 * show_ccset -- displays the special characters settings
 *  args: current_ttyinfo, ccflags
 *  rets: none
 */
{
    // logic to convert a ascii representation of
    // characters assigned to the setting into english
    for (int ccount=0; ccflags[ccount].fl_name != NULL; ccount++)
    {
        if (current_ttyinfo->c_cc[ccflags[ccount].fl_value] <= CNTRLCHARS)
        {
            printf("%s = ^%c ",ccflags[ccount].fl_name,
                (current_ttyinfo->c_cc[ccflags[ccount].fl_value] -1 + 'A'));
        }
        else if (current_ttyinfo->c_cc[ccflags[ccount].fl_value] == SPACE)
        {
            printf("%s = ^? ",ccflags[ccount].fl_name);
        }
        else
        {
            printf("%s = %c ", ccflags[ccount].fl_name,
                    current_ttyinfo->c_cc[ccflags[ccount].fl_value]);
        }
    }
    printf("\n");
}

void show_flagset(struct settermios termiosflg[])
/*
 * show_flagset -- displays the iocl  settings
 *  args: termiosflg
 *  rets: none
 */
{
    for (int i=0; termiosflg[i].location != NULL ; i++)
    {
        if (*(termiosflg[i].location) & termiosflg[i].fl_value)
            printf("%s ",termiosflg[i].fl_name);
        else printf("-%s ",termiosflg[i].fl_name);
    }
    printf("\n");
}



int showbaud(int thespeed, struct baudrate termbaud[])
/*
 * showbaud -- displays the baud rate of terminal
 *  args: thespeed, termbaud
 *  rets: baud rate
 */
{
	int index = 0;
	while (index < BAUD)
	{
	    if (thespeed == (signed int) termbaud[index].baud)
	        break;
	    index++;
	}
    return termbaud[index].rate;;
}

void get_ttyinfo(struct termios *ttydata)
/*
 * get_ttyinfo -- returns the current terminal settings
 *  args: *ttydata
 *  rets: none
 */
{
    if (tcgetattr(0 , ttydata ) == ERROR)
    {
        perror("Error retrieving data from stdin device file");
        exit(1);
    }
}