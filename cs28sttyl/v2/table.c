#include "table.h"
#include <stdlib.h>


setflaginfo ioclsetflags[10] =
{
    {ICRNL, "icrnl", 'i'},
    {ECHO, "echo", 'l'},
    {ECHOE, "echoe", 'l'},
    {ICANON, "icanon", 'l'},
    {ISIG, "isig", 'l'},
    {OPOST, "opost", 'o'},
    {HUPCL, "hupcl", 'c'},
    {VERASE, "erase", 's'},
    {VKILL, "kill", 's'},
    {0,	NULL, 'o'}
};

baudrate termbaud[19] =
{
    {B0, 0},
    {B50, 50},
    {B75, 75},
    {B110, 110},
    {B134, 134},
    {B200, 200},
    {B150, 150},
    {B300, 300},
    {B600, 600},
    {B1200, 1200},
    {B1800, 1800},
    {B2400, 2400},
    {B4800, 4800},
    {B9600, 9600},
    {B19200, 19200},
    {B38400, 38400},
    {B57600, 57600},
    {B115200, 115200},
    {B230400, 230400}
};