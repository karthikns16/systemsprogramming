#include <stdio.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "table.h"

int showbaud(int thespeed, struct baudrate termbaud[]);
void show_flagset(struct termios *current_ttyinfo, struct setflaginfo ioclsetflags[]);
void display_settings(struct setflaginfo ioclsetflags[], struct baudrate termbaud[]);
void set_control_settings(char *name, char key, struct termios ttyinfo);
void get_ttyinfo(struct termios *ttydata);
int set_iocl_settings(char *flag, tcflag_t fl_value, char key);
void validate_arguments(int argc, char **argv, struct setflaginfo ioclsetflags[]);

#define BAUD 19

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        display_settings(ioclsetflags, termbaud);
    }
    else
    {
        //validate_arguments(argc, argv,ioclsetflags);
        int argcount;
        for (argcount = 1; argcount < argc; argcount++)
        {
            int count;
            for (count=0; ioclsetflags[count].fl_value != 0 ; count++)
            {
                if ((strcmp(argv[argcount],ioclsetflags[count].fl_name ) == 0) ||
                    (strcmp(&argv[argcount][1],ioclsetflags[count].fl_name ) == 0))
                {
                    switch (ioclsetflags[count].key)
                    {
                        case 's':
                        {
                            if ((argcount+1 < argc) && (atoi(argv[argcount+1])>=0))
                            {
                                struct termios ccttyinfo;
                                get_ttyinfo(&ccttyinfo);
                                ccttyinfo.c_cc[ioclsetflags[count].fl_value] = *argv[argcount+1];
                                tcsetattr(0, TCSANOW, &ccttyinfo);
                                argcount=argcount+1;
                            }
                            else
                            {
                                fprintf(stderr, "sttyl not enough arguments:'%s'\n", argv[argcount]);
                                return 1;
                            }
                            break;
                        }
                        default:
                        {
                            set_iocl_settings(argv[argcount], ioclsetflags[count].fl_value, ioclsetflags[count].key);
                        }
                    }
                }
            }
        }
    }
    return 0;
}

// void validate_arguments(int argc, char **argv, struct setflaginfo ioclsetflags[])
// {
//     int found = 0;
//     for (int argcount = 1; argcount < argc; argcount++)
//     {
//         for (int argindex=0; ioclsetflags[argindex].fl_value != 0 ; argindex++)
//         {
//             if ((strcmp(argv[argcount],ioclsetflags[argindex].fl_name ) == 0) ||
//             (strcmp(&argv[argcount][1],ioclsetflags[argindex].fl_name ) == 0))
//             {
//                 found++;
//                 break;
//             }
//         }
//     }
//     if (found != argc)
//     {
//         fprintf(stderr, "sttyl: has invalid argument\n");
//         exit(1);
//     }
// }

int set_iocl_settings(char *flag, tcflag_t fl_value, char key)
{
    struct termios ioclttyinfo;
    get_ttyinfo(&ioclttyinfo);
    switch (key)
    {
        case 'i':
        {
            if (flag[0] != '-') ioclttyinfo.c_iflag |= fl_value;
            else ioclttyinfo.c_iflag &= ~fl_value;
            tcsetattr(0, TCSANOW, &ioclttyinfo);
            break;
        }
        case 'l':
        {
            if (flag[0] != '-')  ioclttyinfo.c_lflag |= fl_value;
            else ioclttyinfo.c_lflag &= ~fl_value;
            tcsetattr(0, TCSANOW, &ioclttyinfo);
            break;
        }
        case 'o':
        {
            if (flag[0] != '-')  ioclttyinfo.c_oflag |= fl_value;
            else ioclttyinfo.c_oflag &= ~fl_value;
            tcsetattr(0, TCSANOW, &ioclttyinfo);
            break;
        }
        case 'c':
        {
            if (flag[0] != '-')  ioclttyinfo.c_cflag |= fl_value;
            else ioclttyinfo.c_cflag &= ~fl_value;
            tcsetattr(0, TCSANOW, &ioclttyinfo);
            break;
        }
    }
    return 0;
}

void display_settings(struct setflaginfo ioclsetflags[], struct baudrate termbaud[])
{
    struct winsize w;
    if (ioctl(0, TIOCGWINSZ, &w) == -1)
    {
        perror("Error retrieving Window size of the terminal\n");
        exit(1);
    }
    struct termios current_ttyinfo;
    get_ttyinfo(&current_ttyinfo);
    printf("speed %d baud; rows %d; columns %d\n",
                showbaud(cfgetospeed(&current_ttyinfo), termbaud), w.ws_row, w.ws_col);
    printf("intr = ^%c; erase = ^%c; kill = ^%c\n", current_ttyinfo.c_cc[VINTR]-1+'A',
                current_ttyinfo.c_cc[VERASE]-'A'+1, current_ttyinfo.c_cc[VKILL]-1+'A');
    printf("start = ^%c; stop = ^%c; werase = ^%c\n", current_ttyinfo.c_cc[VSTART]-1+'A',
                current_ttyinfo.c_cc[VSTOP]-1+'A', current_ttyinfo.c_cc[VWERASE]-1+'A');

    show_flagset(&current_ttyinfo,ioclsetflags);

}

void show_flagset(struct termios *current_ttyinfo, struct setflaginfo ioclsetflags[])
{
    int i;
    for (i=0; ioclsetflags[i].fl_value != 0 ; i++)
    {
        switch (ioclsetflags[i].key)
        {
            case 'i':
            {
                if (current_ttyinfo->c_iflag & ioclsetflags[i].fl_value) printf("%s ",ioclsetflags[i].fl_name);
                else printf("-%s ",ioclsetflags[i].fl_name);
                break;
            }
            case 'l':
            {
                if (current_ttyinfo->c_lflag & ioclsetflags[i].fl_value) printf("%s ",ioclsetflags[i].fl_name);
                else printf("-%s ",ioclsetflags[i].fl_name);
                break;
            }
            case 'o':
            {
                if (current_ttyinfo->c_oflag & ioclsetflags[i].fl_value) printf("%s ",ioclsetflags[i].fl_name);
                else printf("-%s ",ioclsetflags[i].fl_name);
                break;
            }
            case 'c':
            {
                if (current_ttyinfo->c_cflag & ioclsetflags[i].fl_value) printf("%s ",ioclsetflags[i].fl_name);
                else printf("-%s ",ioclsetflags[i].fl_name);
                break;
            }
        }
    }
    printf("\n");
}


int showbaud(int thespeed, struct baudrate termbaud[])
{
	int index = 0;
	while (index < BAUD)
	{
	    if (thespeed == (signed int) termbaud[index].baud)
	    {
	        return termbaud[index].rate;
	    }
	    index++;
	}
	return 0;
}

void get_ttyinfo(struct termios *ttydata)
{
    if (tcgetattr(0 , ttydata ) == -1)
    {
        perror("Error retrieving data from stdin device file");
        exit(1);
    }
}