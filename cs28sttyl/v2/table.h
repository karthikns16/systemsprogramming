#include <termios.h>

#ifndef _TABLE_H_
#define _TABLE_H_

typedef struct setflaginfo { tcflag_t fl_value; char *fl_name; char key;} setflaginfo;
setflaginfo ioclsetflags[10];

typedef struct baudrate {speed_t baud; int rate;} baudrate;
baudrate termbaud[19];

#endif