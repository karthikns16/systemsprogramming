#include "table.h"
#include <stdlib.h>

flaginfo iflags[2]=
{
    {ICRNL, "icrnl"},
    {0,	NULL}
};

flaginfo lflags[5]=
{
    {ECHO, "echo"},
    {ECHOE, "echoe"},
    {ICANON, "icanon"},
    {ISIG, "isig"},
    {0,	NULL}
};

flaginfo oflags[2]=
{
    {OPOST, "opost"},
    {0,	NULL}
};

flaginfo cflags[2]=
{
    {HUPCL, "hupcl"},
    {0,	NULL}
};

flaginfo ccsetflags[3] =
{
    {VERASE, "erase"},
    {VKILL, "kill"},
    {0,	NULL}
};

setflaginfo ioclsetflags[10] =
{
    {ICRNL, "icrnl", 'i'},
    {ECHO, "echo", 'l'},
    {ECHOE, "echoe", 'l'},
    {ICANON, "icanon", 'l'},
    {ISIG, "isig", 'l'},
    {OPOST, "opost", 'o'},
    {HUPCL, "hupcl", 'c'},
    {VERASE, "erase", 's'},
    {VKILL, "kill", 's'},
    {0,	NULL, 'o'}
};