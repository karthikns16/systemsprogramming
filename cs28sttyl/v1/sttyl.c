#include <stdio.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "table.h"

#define SUPPORTED 100
#define UNSUPPORTED 0
#define NONE 0

char* showbaud(int thespeed);
void show_flagset(struct termios *current_ttyinfo, struct setflaginfo ioclsetflags[]);
void display_settings(struct setflaginfo ioclsetflags[]);
void set_control_settings(char *name, char key, struct termios ttyinfo);
void get_ttyinfo(struct termios *ttydata);
int set_iocl_settings(char *flag, tcflag_t fl_value, char key);

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        display_settings(ioclsetflags);
    }
    else
    {
        int argcount;
        for (argcount = 1; argcount < argc; argcount++)
        {
            int count;
            for (count=0; ioclsetflags[count].fl_value != 0 ; count++)
            {
                if ((strcmp(argv[argcount],ioclsetflags[count].fl_name ) == 0) ||
                    (strcmp(&argv[argcount][1],ioclsetflags[count].fl_name ) == 0))
                {
                    switch (ioclsetflags[count].key)
                    {
                        case 's':
                        {
                            if ((argcount+1 < argc) && (atoi(argv[argcount+1])>=0))
                            {
                                struct termios ccttyinfo;
                                get_ttyinfo(&ccttyinfo);
                                ccttyinfo.c_cc[ioclsetflags[count].fl_value] = *argv[argcount+1];
                                tcsetattr(0, TCSANOW, &ccttyinfo);
                                argcount=argcount+1;
                            }
                            else
                            {
                                fprintf(stderr, "sttyl not enough arguments:'%s'\n", argv[argcount]);
                                return 1;
                            }
                            break;
                        }
                        default:
                        {
                            set_iocl_settings(argv[argcount], ioclsetflags[count].fl_value, ioclsetflags[count].key);
                        }
                    }
                }
            }
        }
    }
    return 0;
}

int set_iocl_settings(char *flag, tcflag_t fl_value, char key)
{
    struct termios ioclttyinfo;
    get_ttyinfo(&ioclttyinfo);
    switch (key)
    {
        case 'i':
        {
            if (flag[0] != '-') ioclttyinfo.c_iflag |= fl_value;
            else ioclttyinfo.c_iflag &= ~fl_value;
            tcsetattr(0, TCSANOW, &ioclttyinfo);
            break;
        }
        case 'l':
        {
            if (flag[0] != '-')  ioclttyinfo.c_lflag |= fl_value;
            else ioclttyinfo.c_lflag &= ~fl_value;
            tcsetattr(0, TCSANOW, &ioclttyinfo);
            break;
        }
        case 'o':
        {
            if (flag[0] != '-')  ioclttyinfo.c_oflag |= fl_value;
            else ioclttyinfo.c_oflag &= ~fl_value;
            tcsetattr(0, TCSANOW, &ioclttyinfo);
            break;
        }
        case 'c':
        {
            if (flag[0] != '-')  ioclttyinfo.c_cflag |= fl_value;
            else ioclttyinfo.c_cflag &= ~fl_value;
            tcsetattr(0, TCSANOW, &ioclttyinfo);
            break;
        }
    }
    return 0;
}

void display_settings(struct setflaginfo ioclsetflags[])
{
    struct winsize w;
    if (ioctl(0, TIOCGWINSZ, &w) == -1)
    {
        perror("Error retrieving Window size of the terminal\n");
        exit(1);
    }
    struct termios current_ttyinfo;
    get_ttyinfo(&current_ttyinfo);
    printf("%s; rows %d; columns %d\n",
                showbaud(cfgetospeed(&current_ttyinfo )), w.ws_row, w.ws_col);
    // printf("%d, %d, %d\n",current_ttyinfo.c_cc[VINTR],
    //             current_ttyinfo.c_cc[VSTART],current_ttyinfo.c_cc[VERASE]);
    printf("intr = ^%c; erase = ^%c; kill = ^%c\n", current_ttyinfo.c_cc[VINTR]-1+'A',
                current_ttyinfo.c_cc[VERASE]-'A'+1, current_ttyinfo.c_cc[VKILL]-1+'A');
    printf("start = ^%c; stop = ^%c; werase = ^%c\n", current_ttyinfo.c_cc[VSTART]-1+'A',
                current_ttyinfo.c_cc[VSTOP]-1+'A', current_ttyinfo.c_cc[VWERASE]-1+'A');

    show_flagset(&current_ttyinfo,ioclsetflags);

}

void show_flagset(struct termios *current_ttyinfo, struct setflaginfo ioclsetflags[])
{
    int i;
    for (i=0; ioclsetflags[i].fl_value != 0 ; i++)
    {
        switch (ioclsetflags[i].key)
        {
            case 'i':
            {
                if (current_ttyinfo->c_iflag & ioclsetflags[i].fl_value) printf("%s ",ioclsetflags[i].fl_name);
                else printf("-%s ",ioclsetflags[i].fl_name);
                break;
            }
            case 'l':
            {
                if (current_ttyinfo->c_lflag & ioclsetflags[i].fl_value) printf("%s ",ioclsetflags[i].fl_name);
                else printf("-%s ",ioclsetflags[i].fl_name);
                break;
            }
            case 'o':
            {
                if (current_ttyinfo->c_oflag & ioclsetflags[i].fl_value) printf("%s ",ioclsetflags[i].fl_name);
                else printf("-%s ",ioclsetflags[i].fl_name);
                break;
            }
            case 'c':
            {
                if (current_ttyinfo->c_cflag & ioclsetflags[i].fl_value) printf("%s ",ioclsetflags[i].fl_name);
                else printf("-%s ",ioclsetflags[i].fl_name);
                break;
            }
        }
    }
    printf("\n");
}


char* showbaud(int thespeed)
{
	switch (thespeed)
	{
		case B0: return "hang up";
		case B50: return "speed 50 baud";
		case B75: return "speed 75 baud";
		case B110: return "speed 110 baud";
		case B134: return "speed 134 baud";
		case B200: return "speed 200 baud";
		case B150: return "speed 150 baud";
		case B300: return "speed 300 baud";
		case B600: return "speed 600 baud";
		case B1200: return "speed 1200 baud";
		case B1800: return "speed 1800 baud";
		case B2400: return "speed 2400 baud";
		case B4800: return "speed 4800 baud";
		case B9600: return "speed 9600 baud";
		case B19200: return "speed 19200 baud";
		case B38400: return "speed 38400 baud";
		case B57600: return "speed 57600 baud";
		case B115200: return "speed 115200 baud";
		case B230400: return "speed 230400 baud";
		default: return "Fast";
	}
}

void get_ttyinfo(struct termios *ttydata)
{
    if (tcgetattr(0 , ttydata ) == -1)
    {
        perror("Error retrieving data from stdin device file");
        exit(1);
    }
}