#include <termios.h>
#ifndef _TABLE_H_
#define _TABLE_H_

typedef struct flaginfo { tcflag_t fl_value; char *fl_name;} flaginfo;
typedef struct setflaginfo { tcflag_t fl_value; char *fl_name; char key;} setflaginfo;
flaginfo iflags[2];
flaginfo lflags[5];
flaginfo oflags[2];
flaginfo cflags[2];
flaginfo ccsetflags[3];
setflaginfo ioclsetflags[10];

#endif