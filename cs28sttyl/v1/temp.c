int set_iocl_settings(char *flag, struct setflaginfo ioclsetflags[])
{
    int i, found = 0;
    struct termios ioclttyinfo;
    char temp[strlen(flag)-1];
    get_ttyinfo(&ioclttyinfo);
    if (flag[0] == '-')
    {

        temp[strlen(flag)] = '\0';
        strncpy(temp, &flag[1], strlen(flag));
        //printf("flag %s\n", temp);

    }
    for (i=0; ioclsetflags[i].fl_value != 0 ; i++)
    {
        if ((strcmp(flag, ioclsetflags[i].fl_name) == 0) || (strcmp(temp,ioclsetflags[i].fl_name) == 0))
        {
            switch (ioclsetflags[i].key)
            {
                case 'i':
                {
                    if (flag[0] != '-') ioclttyinfo.c_iflag |= ioclsetflags[i].fl_value;
                    else ioclttyinfo.c_iflag &= ~ioclsetflags[i].fl_value;
                    tcsetattr(0, TCSANOW, &ioclttyinfo);
                    break;
                }
                case 'l':
                {
                    if (flag[0] != '-')  ioclttyinfo.c_lflag |= ioclsetflags[i].fl_value;
                    else ioclttyinfo.c_lflag &= ~ioclsetflags[i].fl_value;
                    tcsetattr(0, TCSANOW, &ioclttyinfo);
                    break;
                }
                case 'o':
                {
                    if (flag[0] != '-')  ioclttyinfo.c_oflag |= ioclsetflags[i].fl_value;
                    else ioclttyinfo.c_oflag &= ~ioclsetflags[i].fl_value;
                    tcsetattr(0, TCSANOW, &ioclttyinfo);
                    break;
                }
                case 'c':
                {
                    if (flag[0] != '-')  ioclttyinfo.c_cflag |= ioclsetflags[i].fl_value;
                    else ioclttyinfo.c_cflag &= ~ioclsetflags[i].fl_value;
                    tcsetattr(0, TCSANOW, &ioclttyinfo);
                    break;
                }
            }
            found++;
            break;
        }
    }
    if (found == UNSUPPORTED)
    {
        fprintf(stderr, "sttyl invalid argument:'%s' unsupported\n", flag);
        return 1;
    }

    return 0;
}