-----------------------------------------------------------
Code by: Karthik NS(kan605@g.harvard.edu)
Date: 02/20/2018
History: v1
-----------------------------------------------------------

Purpose:

The purpose of tarc is implement a version of tar
by adding following features:

   [a] creates a tar file by name supplied in argv[1]
   [b] tar files, directories, symlinks and fifo


Layering:

	The program will tar files provided as input through command line
	1st argument - name of tar file name
	2nd to nth argument - files, directories, fifo's, symlink to be added into the tar file

	The program implements a simple recursion to traverse through the directory tree
	The program will ignore any pathname which are greater than 100 characters
	The program will ignore files/directories which are not readable
	The program will exit in case of failure to read a file's data, write a file data & header into the tar

	main() -- for each arguments determines if input is directory or other file types, calls:
			recursdir() -- if the file is directory then it recurses through the directory to determine the contents of the directory, calls
				writetar() -- prepare the block of header and data that has to be added into the tar file and calls
			statfunc() -- obtains the stat information of the file/directory/symlink/fifo
			writeEOF() -- writes the 1024 '\0' indicate the end of the archive
		writetar	-- prepare the block of header and data that has to be added into the tar file and calls
		    statfunc() -- obtains the stat information of the file/directory/symlink/fifo
		    verifyaccess() -- verifies access to file/directory/symlink/fifo and skips if no access
		    prepareblock() -- function to copy the formatted data into the datablock
		    preparenumblock() -- function to copy the formatted data (integers) into the datablock
			 uid_to_name() -- function to identify the user name owning the file/directory/symlink/fifo
			 gid_to_name() -- function to identify the group name owning the file/directory/symlink/fifo
			 cksum() -- returns the checksum of the file/directory/symlink/fifo added to the tar
			 open_file() -- opens a file using the filename
			 read_file() -- reads the data into buffer of the given file descriptor
			 write_file() -- writes the data into buffer datablock of 512 bytes


File Structure:

	tarc.c			-- main program that generates the tar file

	/* tarc.c  - main program that generates the tar file
	 *
	 *
	 *  functions are
	 *      void prepareblock(char *datablock, int base, char *values, int len)             - prepare data block which are strings for the tar header
	 *      void preparenumblock(char *datablock, int base, int value, int len, char *fmt)  - prepare data block which are number for the tar header
	 *      int checksum(char *datablock)                                                   - return checksum of the file being tar'ed
	 *      int writeEOF(int fd)                                                            - writes the 1024 '\0' indicate the end of the archive
	 *      int recursdir(char *dirname, int fd)                                            - recurse through directory structure
	 *      int writetar(char *filename, int fd)                                            - create the tar file
	 *      int statfunc(char *filename, struct stat *statvalue)                            - returns the lstat of any object sent to function
	 *      int verifyaccess(char *filename, struct stat filestat)                          - verifies READ is available for file and READ/EXECUTE on directory
	 *
	 */

	filehandler.c	-- helper program to open, read, write and close file.

	/* filehandler.c  - functions to open, read, write and close file.
	 *                  functions to provide the owner name and group name for the file or directory
	 *
	 *      functions are
	 *              char *uid_to_name( uid_t uid )  - returns the user id or user name of the uid passed
	 *              char *gid_to_name( gid_t gid )  - returns the group id or group name of the gid passed
	 *              int open_file(char *filename)   - return file descriptor after opening the file
	 *              int close_file(int fd)          - closes a file
	 *                  return 1 on error
	 *              int write_file(int fd, char *data, int len) - writes data to file mentioned in the file descriptor
	 *                  return 0 on success
	 *                  exits on error
	 *              int read_file(int fd, char *data, int len) - read data to file mentioned in the file descriptor in to buffer
	 *                  return number of character read into buffer
	 *                  exits on error
	 *
	 */