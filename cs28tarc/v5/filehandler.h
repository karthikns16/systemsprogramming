//
// Created by KNS on 2/3/18.
//

#ifndef FILEHANDLER_H
#define FILEHANDLER_H




#include <stdio.h>
#include <pwd.h>
#include <grp.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
/*
 * declarations for code in helper.c
 */

char *uid_to_name( uid_t uid );
char *gid_to_name(gid_t gid);
int open_file(char *filename);
int close_file(int fd);
int write_file(int fd, char *data, int len);
int read_file(int fd, char *data, int len);

#endif