/* filehandler.c  - functions to open, read, write and close file.
 *                  functions to provide the owner name and group name for the file or directory
 *
 *      functions are
 *              char *uid_to_name( uid_t uid )  - returns the user id or user name of the uid passed
 *              char *gid_to_name( gid_t gid )  - returns the group id or group name of the gid passed
 *              int open_file(char *filename)   - return file descriptor after opening the file
 *              int close_file(int fd)          - closes a file
 *                  return 1 on error
 *              int write_file(int fd, char *data, int len) - writes data to file mentioned in the file descriptor
 *                  return 0 on success
 *                  exits on error
 *              int read_file(int fd, char *data, int len) - read data to file mentioned in the file descriptor in to buffer
 *                  return number of character read into buffer
 *                  exits on error
 *
 */



#include <stdio.h>
#include <pwd.h>
#include <grp.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>


/*
 * uid_to_name -- returns the user id or user name of the uid passed
 *  args: stat.st_uid
 *  rets: none if error, user id or user name of the uid passed
 */

char *uid_to_name( uid_t uid )
{
    struct passwd *uid_ptr;                                 // pointer to hold the user details
    static char numstr[10];                                 // string to hold the uid in case uname is not present
    if ( (uid_ptr=getpwuid(uid)) == NULL)
    {
        sprintf(numstr, "%d",uid);
        return numstr;
    }
    return uid_ptr->pw_name;                                // returns the uname
}

/*
 * gid_to_name -- returns the group id or group name of the gid passed
 *  args: stat.st_gid
 *  rets: none if error, group id or group name of the gid passed
 */

char *gid_to_name( gid_t gid )
{
    struct group *gid_ptr;                                  // pointer to hold the group details
    static char numstr[10];                                 // string to hold the gid in case gname is not present
    if ( (gid_ptr=getgrgid(gid)) == NULL)
    {
        sprintf(numstr,"%d",gid);
        return numstr;
    }
    return gid_ptr->gr_name;                                // returns the gname
}

/*
 * open_file -- opens the file
 *  args: file name
 *  rets: file descriptor
 */

int open_file(char *filename)
{
    int fd = open(filename, O_RDONLY);                      // opens the file
    return fd;                                              // returns the file descriptor
}

/*
 * close_file -- closes the file
 *  args: file descriptor
 *  rets: 0 on success, 1 on error
 */


int close_file(int fd)
{
    if (fd != -1)
        return close(fd);
    return 1;
}

/*
 * write_file -- writes data to buffer
 *  args: file descriptor, char buffer, length of data to be written to buffer
 *  rets: 0 on success, exits on error
 */

int write_file(int fd, char *data, int len)
{
    if (write(fd, data, len) == -1)                             // write data into buffer
    {
        fprintf(stderr, "unable to write data to tar\n");
        close_file(fd);
        exit(1);
    }
    return 0;
}

/*
 * read_file -- read data into buffer
 *  args: file descriptor, char buffer, length of data to be read into buffer
 *  rets: number of bytes read on success, exits on error
 */

int read_file(int fd, char *data, int len)
{
    int nread = 0;
    nread = read(fd, data, len);                             // read data into buffer and store the length of data read
    if (nread == -1)
    {
        fprintf(stderr, "unable to read from data file\n");
        close_file(fd);
        exit(1);
    }
    return nread;                                           // returns the number of bytes read
}