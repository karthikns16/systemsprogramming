/* tarc.c  - main program that generates the tar file
 *
 *
 *  functions are
 *      void prepareblock(char *datablock, int base, char *values, int len)             - prepare data block which are strings for the tar header
 *      void preparenumblock(char *datablock, int base, int value, int len, char *fmt)  - prepare data block which are number for the tar header
 *      int checksum(char *datablock)                                                   - return checksum of the file being tar'ed
 *      int writeEOF(int fd)                                                            - writes the 1024 '\0' indicate the end of the archive
 *      int recursdir(char *dirname, int fd)                                            - recurse through directory structure
 *      int writetar(char *filename, int fd)                                            - create the tar file
 *      int statfunc(char *filename, struct stat *statvalue)                            - returns the lstat of any object sent to function
 *      int verifyaccess(char *filename, struct stat filestat)                          - verifies READ is available for file and READ/EXECUTE on directory
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include "filehandler.h"

#define	BLEN 512

void prepareblock(char *datablock, int base, char *values, int len);
void preparenumblock(char *datablock, int base, int value, int len, char *fmt);

int checksum(char *datablock);
int writeEOF(int fd);
int writetar(char *filename, int fd);
int recursdir(char *dirname, int fd);
int statfunc(char *filename, struct stat *statvalue);
int verifyaccess(char *filename, struct stat filestat);

int success = 0;                                                                        // determines the success or failure of tar program

int main(int argc, char *argv[])
{
    if (argc < 3)                                                                       // determines if the program has correct arguments passed
    {
        fprintf(stderr,"no files to tar or not valid archive name\n");
        exit(1);
    }
    int fd = creat(argv[1], (S_IRWXU));                                                 // creates empty tar file
    if (fd == -1)                                                                       // logic to exit in case if the file creation failed
    {
        fprintf(stderr,"unable to create a tar file\n");
        close_file(fd);
        exit(1);
    }

    int cntfiles = 2;
    while (cntfiles < argc)                                                             // start of the main logic
    {
        struct stat isdircheck;                                                         // variable to identify if the argument passed is a directory
        if (statfunc(argv[cntfiles], &isdircheck) == -1)                                // ignores if the stat fails on the file
        {
            cntfiles++;
            continue;
        }
        if (S_ISDIR(isdircheck.st_mode))                                                // logic that call writetar for writing the directory data into tar
        {
            char tdirname[strlen(argv[cntfiles])+2];
            char tail[2]={'/','\0'};
            strcpy(tdirname, argv[cntfiles]);
            strcat(tdirname, tail);
            success=success+writetar(tdirname, fd);
            success=success+recursdir(argv[cntfiles],fd);
        }
        else
        {
            if (strlen(argv[cntfiles])>100)
            {
                cntfiles++;
                continue;
            }
            success=success+writetar(argv[cntfiles], fd);
        }
        cntfiles++;
    }
    success=success+writeEOF(fd);                                                       // call writeEOF to write 1024 '\0' bytes to end of tar
    if (success > 0)
    {
        fprintf(stderr, "tar generation failed with return code %d\n", success);
    }
    return success;
}

/*
 * recursdir -- recursion to access directory and write the files inside a directory to the tar file
 *  args: director name, file descriptor the tar file
 *  rets: 0 on success, 1 on error
 */

int recursdir(char *dirname, int fd)
{
    DIR *dp;                                                        // pointer to point directory information
    dp = opendir(dirname);                                          // opens directory
    struct dirent *dirval;                                          // extract the inode and directory name into struct
    if (!dp)                                                        // handle errors in case of issue with opening directory
    {
        fprintf(stderr, "unable to open directory to check for files inside them\n");
        closedir(dp);
        return 1;
    }
    while ((dirval=readdir(dp)) != NULL)                            // recursion logic to loop through the directory
    {
        if (strcmp(dirval->d_name, ".") != 0 && strcmp(dirval->d_name, "..") != 0)      // filters . and .. from the list of files inside a directory
        {
            int dlength=strlen(dirname)+strlen(dirval->d_name);                         // hold the length of the path
            if (dlength > 98)                                                           // ignore any pathname which greater than 100 characters long
            {
                continue;
            }
            char newpath[dlength+2];
            sprintf(newpath,"%s/%s", dirname, dirval->d_name);
            struct stat isdir;                                                          // struct to check if the current file is a directory
            if (statfunc(newpath, &isdir) == -1)
            {
                closedir(dp);
                return 1;
            }
            if (S_ISDIR(isdir.st_mode))
            {
                char tdirname[strlen(newpath)+2];                                   // variable to hold the directory found inside the current directory
                char tail[2]={'/','\0'};
                strcpy(tdirname, newpath);
                strcat(tdirname, tail);
                writetar(tdirname, fd);
                recursdir(newpath, fd);
            }
            else
            {
                writetar(newpath, fd);
            }
        }
    }
    closedir(dp);
    return 0;
}

/*
 * writetar -- writes data into tar file (header and data)
 *  args: filename name, file descriptor the tar file
 *  rets: 0 on success, 1 on error
 */


int writetar(char *filename, int fd)
{
    struct stat filestat;                               // variable to stat a file
    if (statfunc(filename, &filestat) == -1)            // stat the file
        return 1;
    if (verifyaccess(filename, filestat) != 0)          // verifies access ot the file
        return 1;
    char datablock[BLEN];                               // create the datablock of 512 chars

    memset(datablock, '\0', (BLEN));
    if (S_ISLNK(filestat.st_mode))
    {
        /*
            logic to find and update the linked path to the tar file
        */
        char linkname[100];
        memset(linkname, '\0', 100);
        //readlink(filename, linkname, 100);
        int linklen=0;
        linklen=readlink(filename, linkname, 101);
        if (linklen == 101)
            return 0;
        prepareblock(datablock, 156, "2", 1);
        prepareblock(datablock, 157, linkname, 100);
    }
    /* prepareblock or preparenumblock is used to add data into the datablock buffer
       finally the data block is added into the tar file
    */
    prepareblock(datablock, 155, " ", 1);
    prepareblock(datablock, 257, "ustar", 12);
    prepareblock(datablock, 262, " ", 1);
    prepareblock(datablock, 263, " ", 1);
    prepareblock(datablock, 0, filename, strlen(filename));
    // determines the permission of the file to the update to the tar
    int permission = (filestat.st_mode & S_IRWXU) + (filestat.st_mode & S_IRWXG) + (filestat.st_mode & S_IRWXO);

    /* prepareblock or preparenumblock is used to add data into the datablock buffer
       finally the data block is added into the tar file
    */
    preparenumblock(datablock, 100, permission, 8, "%07o");
    preparenumblock(datablock, 108, filestat.st_uid, 8, "%07o");
    preparenumblock(datablock, 116, filestat.st_gid, 8, "%07o");

    if (!S_ISLNK(filestat.st_mode) && !S_ISDIR(filestat.st_mode))
    {
        preparenumblock(datablock, 124, (int) filestat.st_size, 12, "%011o");
    }
    else
    {
        preparenumblock(datablock, 124, 0, 12, "%011o");
    }
    preparenumblock(datablock, 136, (int) filestat.st_mtime, 12, "%011o");
    if (S_ISDIR(filestat.st_mode))  prepareblock(datablock, 156, "5", 1);
    if (S_ISREG(filestat.st_mode))  prepareblock(datablock, 156, "0", 1);
    if (S_ISFIFO(filestat.st_mode)) prepareblock(datablock, 156, "6", 1);
    char *uname, *gname;
    uname=uid_to_name(filestat.st_uid);
    gname=gid_to_name(filestat.st_gid);
    prepareblock(datablock, 265, uname, sizeof(uname));
    prepareblock(datablock, 297, gname, sizeof(gname));
    preparenumblock(datablock, 148, (int) checksum(datablock), 7, "%06o");

    // writes the header datablock into the tar
    write_file(fd, datablock, BLEN);

    // logic for regular file data be loaded into tar file
    // writes the data into the tar
    // writes '\0' if the data in the file is less than 512 bytes
    if ((S_ISREG(filestat.st_mode) && ((int) filestat.st_size) !=0))
    {
        int ofd = open_file(filename);
        if ((ofd == -1))
        {
            fprintf(stderr,"unable to open data file\n");
            return 1;
        }
        int rd=0;
        char buf[BLEN];
        // recursively read the data in the file
        while ((rd = read_file(ofd, buf, BLEN)) > 0)
        {
            write_file(fd, buf, rd);
            if (rd < BLEN)
            {
                char temp[BLEN-rd];
                memset(temp, '\0', (BLEN-rd));
                write_file(fd,temp,(BLEN-rd));
            }
        }
        close_file(ofd);
    }
    return 0;
}

/*
 * prepareblock -- function to copy the formatted data into the datablock
 *  args: data array, position to start writing data, data, length of data to be written
 *  rets: none
 */

void prepareblock(char *datablock, int base, char *values, int len)
{
    strncpy(datablock+(base), values, len);
}

/*
 * preparenumblock -- function to copy the formatted data (integers) into the datablock
 *  args: data array, position to start writing data, data, length of data to be written, octal format
 *  rets: none
 */

void preparenumblock(char *datablock, int base, int value, int len, char *fmt)
{
    char temp[len];
    memset(temp, '\0', len);
    sprintf(temp, fmt, value);
    strncpy(datablock+(base), temp, len);
}

/*
 * checksum -- function determine the checksum of the tar file
 *  args: header block of the file/directory/fifo/symlink
 *  rets: checksum (cksum)
 */

int checksum(char *datablock)
{
    int fsize=0;
    int cksum=0;
    while (fsize < BLEN)
    {
        cksum=cksum+datablock[fsize];
        if ((fsize > 148) && (fsize < 156))
        {
            cksum=cksum+(' ');
        }
        fsize++;
    }
    return cksum;
}

/*
 * writeEOF -- writes the 1024 '\0' indicate the end of the archive
 *  args: file descriptor of tar file
 *  rets: 0 on success
 */

int writeEOF(int fd)
{
    char nullchar[BLEN*2];
    memset(nullchar, '\0', (BLEN*2));
    //int n = BLEN*2;
    write_file(fd, nullchar, BLEN*2);
    // closes the tar file finally
    close_file(fd);
    return 0;
}

/*
 * statfunc -- returns the lstat of any object sent to function
 *  args: filename, address of varible to load the stat data into
 *  rets: 0 on success, -1 on error
 */

int statfunc(char *filename, struct stat *statvalue)
{
    int retval = 0;
    if ((retval = lstat(filename, statvalue)) == -1)
    {
        fprintf(stderr,"Unable to open %s\n", filename);
    }
    return retval;
}

/*
 * verifyaccess -- verifies READ is available for file and READ/EXECUTE on directory
 *  args: filename, stat of the file
 *  rets: 0 on success, 1 on error
 */

int verifyaccess(char *filename, struct stat filestat)
{
    int result=0;
    if (S_ISDIR(filestat.st_mode))
    {
        result = access(filename, R_OK) + access(filename, X_OK);
        if (result != 0)
        {
            fprintf(stderr,"read or execute permission denied on directory %s\n", filename);
            return 1;
        }
    }
    else
    {
        result = access(filename, R_OK);
        if (result !=0)
        {
            fprintf(stderr,"read permission denied on file %s\n", filename);
            return 1;
        }
    }
    return result;
}