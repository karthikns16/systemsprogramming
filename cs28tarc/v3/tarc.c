#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include "filehandler.h"

#define	BLEN 512

void prepareblock(char *datablock, int base, char *values, int len);
void preparenumblock(char *datablock, int base, int value, int len, char *fmt);
int checksum(char *datablock);
void writeEOF(int fd);
void writetar(char *filename, int fd);

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        fprintf(stderr,"no files to tar or not valid archive name\n");
        exit(1);
    }
    int fd = creat(argv[1], (S_IRWXU));
    int cntfiles = 2;
    while (cntfiles < argc)
    {
        writetar(argv[cntfiles], fd);
        cntfiles++;
    }
    writeEOF(fd);
}

void writetar(char *filename, int fd)
{
    struct stat filestat;
    if (lstat(filename, &filestat) == -1)
    {
        fprintf(stderr,"Unable to open file %s\n", filename);
        exit(1);
    }

    char datablock[BLEN];
    memset(datablock, '\0', (BLEN));

    prepareblock(datablock, 155, " ", 1);
    prepareblock(datablock, 257, "ustar", 12);
    prepareblock(datablock, 262, " ", 1);
    prepareblock(datablock, 263, " ", 1);
    prepareblock(datablock, 0, filename, strlen(filename));

    int permission = (filestat.st_mode & S_IRWXU)+(filestat.st_mode & S_IRWXG)+(filestat.st_mode & S_IRWXO);
    preparenumblock(datablock, 100, permission, 8, "%07o");
    preparenumblock(datablock, 108, filestat.st_uid, 8, "%07o");
    preparenumblock(datablock, 116, filestat.st_gid, 8, "%07o");

    if (!S_ISLNK(filestat.st_mode)) preparenumblock(datablock, 124, (int) filestat.st_size, 12, "%011o");
    preparenumblock(datablock, 136, (int) filestat.st_mtime, 12, "%011o");
    if (S_ISDIR(filestat.st_mode)) prepareblock(datablock, 156, "5", 1);
    if (S_ISREG(filestat.st_mode)) prepareblock(datablock, 156, "0", 1);
    if(S_ISFIFO(filestat.st_mode)) prepareblock(datablock, 156, "6", 1);
    if (S_ISLNK(filestat.st_mode))
    {
        char *linkname;
        linkname = malloc(100*(sizeof(char)));
        memset(linkname, '\0', 100);
        readlink(filename, linkname, 100);
        prepareblock(datablock, 156, "2", 1);
        prepareblock(datablock, 157, linkname, 100);
        preparenumblock(datablock, 124, 0, 12, "%011o");
        free(linkname);
    }
    char *uname, *gname;
    uname=uid_to_name(filestat.st_uid);
    gname=gid_to_name(filestat.st_gid);


    prepareblock(datablock, 265, uname, sizeof(uname));
    prepareblock(datablock, 297, gname, sizeof(gname));
    preparenumblock(datablock, 148, (int) checksum(datablock), 7, "%06o");

    write(fd, datablock, BLEN);

    if (S_ISREG(filestat.st_mode))
    {
        int ofd = open_file(filename);
        if (ofd == -1)
        {
            fprintf(stderr,"unable to open data file");
            exit(1);
        }
        int rd=0;
        char buf[BLEN];
        char *temp;

        while ((rd = read(ofd, buf, BLEN)) > 0)
        {
            write(fd, buf, rd);
            if (rd < BLEN)
            {
                temp=malloc((BLEN-rd)*sizeof(char));
                memset(temp, '\0', (BLEN-rd));
                write(fd,temp,(BLEN-rd));
            }
        }
        close_file(ofd);
        free(temp);
    }
}
void prepareblock(char *datablock, int base, char *values, int len)
{
    strncpy(datablock+(base), values, len);
}

void preparenumblock(char *datablock, int base, int value, int len, char *fmt)
{
    char temp[len];
    memset(temp, '\0', len);
    sprintf(temp, fmt, value);
    strncpy(datablock+(base), temp, len);
}

int checksum(char *datablock)
{
    int fsize=0;
    int cksum=0;
    while (fsize < BLEN)
    {
        cksum=cksum+datablock[fsize];
        if ((fsize > 148) && (fsize < 156))
        {
            cksum=cksum+(' ');
        }
        fsize++;
    }
    return cksum;
}

void writeEOF(int fd)
{
    char dummy[BLEN*2];
    memset(dummy, '\0', (BLEN*2));
    int n = BLEN*2;
    if ((write(fd, dummy, BLEN*2)) != n)
        fprintf(stderr, "unable to write end of file for tar");
        close_file(fd);
        exit(1);
    close_file(fd);
    exit(0);
}