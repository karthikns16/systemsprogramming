#include <stdio.h>
#include <pwd.h>
#include <grp.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

char *uid_to_name( uid_t uid )
{
    struct passwd  *uid_ptr;
    static char numstr[10];
    if ( (uid_ptr=getpwuid(uid)) == NULL)
    {
        sprintf(numstr, "%d",uid);
        return numstr;
    }
    return uid_ptr->pw_name;
}

char *gid_to_name( gid_t gid )
{
    struct group  *gid_ptr;
    static char numstr[10];
    if ( (gid_ptr=getgrgid(gid)) == NULL)
    {
        sprintf(numstr,"%d",gid);
        return numstr;
    }
    return gid_ptr->gr_name;
}

int open_file(char *filename)
{
    int fd = open(filename, O_RDONLY);
    return fd;
}

int close_file(int fd)
{
    if (fd != -1)
        return close(fd);
    return -1;
}

int write_file(char *buf)
{

   return 0;
}

char *read_file(int fd)
{
    return "hello";
}