#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#define ndir 2

//void listdir(char *dirname);
void recursdir(char *dirname);

//struct dirent *tdirval;

int main(int argc, char *argv[])
{
    //listdir(argv[1]);
    recursdir(argv[1]);
    // int i = 0;
    // for (i = 0; i < sizeof(tdirval)/sizeof(struct dirent); i++)
    // {
    //     printf("%d, %s %d\n", (int)tdirval[i].d_ino, tdirval[i].d_name, tdirval[i].d_reclen);
    // }
    // free(tdirval);
    return 0;
}

// void listdir(char *dirname)
// {
//     DIR *dp;
//     struct dirent *dirval;
//     tdirval = malloc(ndir * sizeof *tdirval);
//     dp = opendir(dirname);
//     int count = 0;
//     int prg = 0;
//     while ((dirval=readdir(dp)) != NULL)
//     {
//         if (strcmp(dirval->d_name, ".") != 0 && strcmp(dirval->d_name, "..") != 0)
//         {
//             if (strlen(dirval->d_name)>99)
//             {
//                 continue;
//             }
//             tdirval[prg] = *dirval;
//             count++;
//             prg++;
//             if (count == ndir)
//             {
//                 tdirval = realloc(tdirval, ndir * prg *sizeof *tdirval);
//             }
//         }
//     }
//     free(dirval);
//     closedir(dp);
// }


void recursdir(char *dirname)
{
    DIR *dp;
    dp = opendir(dirname);
    struct dirent *dirval;
    if (!dp)
    {
        fprintf(stderr, "unable to open directory");
    }
    while ((dirval=readdir(dp)) != NULL)
    {
        if (strcmp(dirval->d_name, ".") != 0 && strcmp(dirval->d_name, "..") != 0)
        {
            int nlength=strlen(dirname)+strlen(dirval->d_name);
            if (nlength > 98)
            {
                continue;
            }
            char newpath[nlength+2];
            sprintf(newpath,"%s/%s", dirname, dirval->d_name);
            printf("%s\n", newpath);
            struct stat isdir;
            if (lstat(newpath, &isdir) == -1)
            {
                fprintf(stderr,"Unable to open file %s\n", newpath);
            }

            if (S_ISDIR(isdir.st_mode))
            {
                recursdir(newpath);
            }
        }
    }
    free(dirval);
    closedir(dp);
}

// struct stat info;
// if (lstat(dirval->d_name, &info) == -1)
// {
//     fprintf(stderr,"Unable to open file %s\n", filename);
//     continue;
// }
// if (S_ISLNK(info.st_mode))
// {
//     char *linkname;
//     linkname = malloc(100*(sizeof(char)));
//     memset(linkname, '\0', 100);
//     readlink(dirval->d_name, linkname, 100);
//     if (linkname[100] == '\0')
//         continue;
// }