#include	<stdio.h>
#include	<stdlib.h>

/*
 *
			Offset Length
 -------------------------------------
 name 0 100
 mode 100 8
 uid 108 8
 gid 116 8
 size 124 12
 mtime 136 12
 chksum 148 8
 typeflag 156 1
 linkname 157 100
 magic 257 6
 version 263 2
 uname 265 32
 gname 297 32
 devmajor 329 8
 devminor 337 8
 prefix 345 155
 -------------------------------------
 */
#define	BT_HEAD	0
#define	BT_DATA	1
#define	BLEN	512

main(int ac, char *av[])
{
	int	fd;

	if ( ac == 1 )
		show_archive(0);
	else {
		if ( ( fd = open(av[1], 0) ) >= 0 ){
			show_archive(fd);
			close(fd);
		}
		else {
			perror(av[1]);
			exit(1);
		}
	}
}

show_archive(int fd)
{
	char	b[BLEN];
	int	blocktype = BT_HEAD;

	while( read(fd, b, BLEN) == BLEN )
	{
		showblock(b);
	}
}

showblock(char *b)
{
	show_header(b);
}

/*
 * dump the fields in the header:
 */

show_header(char *b)
{
	showfld(b, 0, 100, "name");
	showfld(b, 100, 8, "mode");
	showfld(b, 108, 8, "uid");
	showfld(b, 116, 8, "gid");
	showfld(b, 124, 12, "size");
	showfld(b, 136, 12, "mtime");
	showfld(b, 148, 8, "chksum");
	showfld(b, 156, 1, "typeflag");
	showfld(b, 157, 100, "linkname");
	showfld(b, 257, 6, "magic");
	showfld(b, 263, 2, "version");
	showfld(b, 265, 32, "uname");
	showfld(b, 297, 32, "gname");
	showfld(b, 329, 8, "devmajor");
	showfld(b, 337, 8, "devminor");
	showfld(b, 345, 155, "prefix");
	hrule();
}

hrule()
{
	int	i;

	for(i=0; i<50; i++)
		putchar('-');
	putchar('\n');
}
showfld(char *b, int base, int len, char *name)
{
	int	decval;

	printf("%12s: ", name);
	if ( len == 1 ){
		putchar(b[base]);
		putchar('\n');
	}
	else {
		sscanf(b+base, "%o", &decval);
		printf("%-*.*s (%d)\n", len, len, b+base, decval);
	}
}