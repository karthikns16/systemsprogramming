#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
    // char name[100];
    char mode[8];
    mode[7]='\0';
    char uid[8];
    uid[7]='\0';
    char gid[8];
    gid[7]='\0';
    char size[12];
    size[11]='\0';
    char mtime[12];
    mtime[11]='\0';
    char cksumf[8];
    //cksumf[7]='\0';
    // char typeflag[1];
    // char linkname[100];
    // char magic[6];
    // char version[2];
    // char uname[32];
    // char gname[32];
    // char devmajor[8];
    // char devminor[8];
    // char prefix[155];

    char datablock[512];
    int csize = 0;
    char dummy[512];
    while (csize < 512)
    {
        datablock[csize] = '\0';
        dummy[csize]='\0';
        csize++;
    }

    if (argc == 1)
    {
        fprintf(stderr,"No files to tar\n");
        exit(1);
    }
    //printf("%s\n", argv[1]);
    struct stat filestat;
    if (lstat(argv[1], &filestat) == -1)
    {
        fprintf(stderr,"Unable to open file %s\n", argv[1]);
        exit(1);
    }
    strncpy(datablock,argv[1],strlen(argv[1]));
    int count=strlen(argv[1]);
    while (count < 100)
    {
      datablock[count] = '\0';
      count++;
    }
    printf("User Id: %o\n", filestat.st_uid);
    sprintf(uid, "%07o", filestat.st_uid);
    strncpy(datablock+(108),uid, 8);
    printf("Group Id: %o\n", filestat.st_gid);
    sprintf(gid, "%07o", filestat.st_gid);
    strncpy(datablock+(116),gid, 8);
    printf("Inode: %o\n", (int) filestat.st_ino);
    int permission = (filestat.st_mode & S_IRWXU)+(filestat.st_mode & S_IRWXG)+(filestat.st_mode & S_IRWXO);


    sprintf(mode, "%07o", permission);
    printf("Permission: %s\n", mode);
    strncpy(datablock+(100),mode, 8);
    strncpy(datablock+(155)," ", 1);
    strncpy(datablock+(156),"0", 1);




    printf("Size: %o\n", (int) filestat.st_size);
    sprintf(size, "%011o", (int) filestat.st_size);
    strncpy(datablock+(124),size, 12);
    printf("Modified Time:%o\n", (int) filestat.st_mtime);
    sprintf(mtime, "%011o", (int) filestat.st_mtime);
    strncpy(datablock+(136),mtime, 12);
    strncpy(datablock+(257), "ustar", 5);
    strncpy(datablock+(262)," ", 1);
    strncpy(datablock+(263)," ", 1);
    //strncpy(datablock+(263), "00", 2);
    strncpy(datablock+(265), "ubuntu", 6);
    strncpy(datablock+(297), "ubuntu", 6);
    switch (filestat.st_mode & S_IFMT) {
           case S_IFBLK:  printf("block device\n");            break;
           case S_IFCHR:  printf("character device\n");        break;
           case S_IFDIR:  printf("directory\n");               break;
           case S_IFIFO:  printf("FIFO/pipe\n");               break;
           case S_IFLNK:  printf("symlink\n");                 break;
           case S_IFREG:  printf("regular file\n");            break;
           case S_IFSOCK: printf("socket\n");                  break;
           default:       printf("unknown?\n");                break;
           }

    printf("%s, %s, %s, %s, %s, %s, %s, %s\n", datablock+100, datablock+108, datablock+116, datablock+124, datablock+136, datablock+257, datablock+(265), datablock+(297) );

    int fsize=0;
    int cksum=0;
    while (fsize <(148))
    {
        cksum=cksum+datablock[fsize];
        fsize++;
    }
    fsize=156;
    while (fsize <(512))
    {
        cksum=cksum+datablock[fsize];
        fsize++;
    }
    cksum=cksum+(' ')*8;
    printf("%d\n", cksum);
    sprintf(cksumf, "%06o", cksum);
    strncpy(datablock+(148), cksumf, 7);

    int fd = creat("test.tar", O_RDWR);
    write(fd, datablock, 512);
    write(fd, dummy, 512);
    write(fd, dummy, 512);

    close(fd);
    return 0;

    // tar you write everything as octal
    // stat can %o ints and floats into octal
}

