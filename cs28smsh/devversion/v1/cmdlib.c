#include	<stdio.h>
#include	<string.h>
#include	<ctype.h>
#include	<stdlib.h>
#include    <unistd.h>
#include    <limits.h>
#include    "builtin.h"
#include    "splitline.h"
#include	"flexstr.h"
#include    "varlib.h"

#define MAXCMD 4

struct cmds {
    char *cmd;
    int fval;
};


static int find_item( char *name);
static void changedir(char *name);
static void readvar(char **name);
static void exitsh(char **name);

static struct cmds ctab[MAXCMD] =
{
    {"cd", 1},
    {"read", 2},
    {"exit", 3},
    {NULL, 0}
};


int CMDlookup( char **name )
/*
 * returns value of var or empty string if not there
 */
{
	int func;

	if ( (func = find_item(name[0]) ) != 0 ) {
        switch(func) {
            case 1:
                changedir(name[1]);
                break;
            case 2:
                readvar(name);
                break;
            case 3:
                exitsh(name);
                break;
            default:
                break;
        }
		return 1;
	}
	return 0;
}


static int find_item( char *name)
/*
 * searches table for an item
 * returns ptr to struct or NULL if not found
 * OR if (first_blank) then ptr to first blank one
 */
{
	int	i;
	int	len ;
	char	*s;

	if ( name == NULL )
		return 0;

	len = strlen(name);

	for( i = 0 ; i < MAXCMD  && ctab[i].cmd != NULL; i++ )
	{
		s = ctab[i].cmd;
		if ( strncmp(s,name,len) == 0 ){
			return ctab[i].fval;
		}
	}
	return 0;
}

static void changedir(char *name)
{
    int rv = 1;
    char *temp;
    if (name == NULL)
        temp=VLlookup("HOME");
    else
        temp=name;
    if ((rv = chdir(temp))!= 0)
        perror("error while changing dir");
}

static void readvar(char **name)
{
    if ((name[1] == NULL) || (!okname(name[1])))
        return;
    char *value;
    int i = 0;
    char c;
    value=emalloc(sizeof(char)*1);
    while ((c=getc(stdin)) != '\n')
    {
        value[i] = c;
        i++;
        value=erealloc(value, sizeof(char)*1);
    }
    value[i] = '\0';
    VLstore(name[1], value);
    free(value);
}

static void exitsh(char **status)
{
    if (!status[1])
        exit(0);
    int i;
    int error = 0;
    for (i = 0; i < strlen(status[1]); i++)
    {
        if (!isdigit(status[1][i]))
        {
            fprintf(stderr,"exit: Illegal number: %s\n", status[1]);
            error = 1;
            break;
        }
    }
    if (!error)
        exit(atoi(status[1]));
}