#ifndef	PROCESS_H
#define	PROCESS_H
// Made changes to the process.c
//int process(char **args);
int process(char *args[], int line);
//int do_command(char **args);
int do_command(char **args, int line);
//int execute(char **args);
int execute(char *argv[], int line);

extern int exit_status;
#endif
