#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<signal.h>
#include	<sys/wait.h>
#include	<string.h>
#include	<ctype.h>
#include    "splitline.h"
#include    "varlib.h"
#include    "flexstr.h"

int substitute(char *args)
{
    int i;

    FLEXSTR newstring;
	fs_init(&newstring, 0);

    for (i=0; i < strlen(args); i++)
    {
        int start = 0;
        int end = 0;
        int y=0;

        if ((args[i] == '$') && (args[i+1] == '$'))
        {
            char datavar[5];
            sprintf(datavar, "%d", getpid());
            fs_addstr(&newstring, datavar);
            i++;
        }
        else if ((args[i] == '$') && (isdigit(args[i+1])))
        {

            fs_addstr(&newstring, VLlookup( &args[i+1] ));
            i++;
        }

        else if (( args[i] == '$' ))
        {
            i++;
            start = i;

            while (i < strlen(args))
            {
                char c = args[i];
                if (c == '$')
                {
                    end = i;
                    i--;
                    break;
                }
                else if (isdigit(c) || !(isalpha(c)))
                {
                    end = i;
                    y = 1;
                    break;
                }
                i++;
                end = i;
            }
            char *varstring;
            char *newstr;

            varstring=emalloc(end-start);
            memset(varstring, '\0', (end-start)+1);
            strncpy(varstring, &args[start], (end-start));
            printf("varstring %s\n", varstring);
            newstr=VLlookup( varstring );
            if (newstr == NULL)
                newstr = "";
            fs_addstr(&newstring, newstr);
            if ((y))
                fs_addch(&newstring, args[i]);
            free(varstring);
        }
        else
        {
            fs_addch(&newstring, args[i]);
        }
    }
    fs_addch(&newstring, '\0');
    printf("new command %s\n",  fs_getstr(&newstring));

    return 0;
}

