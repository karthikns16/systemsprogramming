#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<signal.h>
#include	<string.h>
#include	<sys/wait.h>
#include	<errno.h>
#include	"smsh.h"
#include	"builtin.h"
#include	"varlib.h"
#include	"controlflow.h"
#include	"process.h"
#include	"exitstatus.h"


/* process.c
 * command processing layer: handles layers of processing
 *
 * The process(char **arglist) function is called by the main loop
 * It sits in front of the do_command function which sits
 * in front of the execute() function.  This layer handles
 * two main classes of processing:
 *	a) process - checks for flow control (if, while, for ...)
 * 	b) do_command - does the command by
 *		         1. Is command built-in? (exit, set, read, cd, ...)
 *                       2. If not builtin, run the program (fork, exec...)
 *                    - also does variable substitution (should be earlier)
 */

static int exit_status = 0;
static int signaled = 0;
static int childpid = 0;
static char child_status[100];
static int alarmsig = 0;

int process(char *args[])
/*
 * purpose: process user command: this level handles flow control
 * returns: result of processing command
 *  errors: arise from subroutines, handled there
 */
{
	int		rv = 0;

	if ( args[0] == NULL )
		rv = 0;
	else if ( is_control_command(args[0]) )
		rv = do_control_command(args);
	else if ( ok_to_execute() )
		rv = do_command(args);
	exit_status=rv;
	return rv;
}

/*
 * do_command
 *   purpose: do a command - either builtin or external
 *   returns: result of the command
 *    errors: returned by the builtin command or from exec,fork,wait
 *
 */
int do_command(char **args)
{
	void varsub(char **);
	int  is_builtin(char **, int *);
	int  rv;

	if ( is_builtin(args, &rv) )
		return rv;
	rv = execute(args);
	return rv;
}

int execute(char *argv[])
/*
 * purpose: run a program passing it arguments
 * returns: status returned via wait, or -1 on error
 *  errors: -1 on fork() or wait() errors
 */
{

	extern char **environ;		/* note: declared in <unistd.h>	*/
	int	pid ;
	int	child_info = -1;
	int should_wait = 1;
	int argcount=0;
	while (argv[argcount] != '\0')
		argcount++;

	if ( argv[0] == NULL )		/* nothing succeeds		*/
		return 0;

	if ( strcmp(argv[argcount-1], "&" ) == 0)
	{
		argv[argcount-1]='\0';
		should_wait = 0;
	}
	if ( (pid = fork())  == -1 )
		perror("fork");
	else if ( pid == 0 ){
		environ = VLtable2environ();
		setsignals();
		execvp(argv[0], argv);
		exit_status=exitstatus(errno);
		exit(exit_status);
	}
	else {
		if (should_wait)
		{
			if ( wait(&child_info) == -1 )
				perror("wait");
			else if (signaled)
				return child_info + 128;
		}
		else
		{
			printf("%d\n", pid);
			childpid=pid;
		}
	}
	return WEXITSTATUS(child_info);
}

int execute_cmd(char *argv[])
{
	extern char **environ;
	environ = VLtable2environ();
	setsignals();
	execvp(argv[1], &argv[1]);
	exit_status=exitstatus(errno);
	if (signaled)
		return exit_status + 128;
	exit(exit_status);
}

void setsignals()
{
	signal (SIGCHLD, proc_exit);
	signal (SIGINT, sig_int);
	signal (SIGQUIT, SIG_DFL);
	signal (SIGALRM, SIG_DFL);
}

int get_exit_status()
{
	return exit_status;
}

void set_exit_status(int n)
{
	exit_status=n;
}

void set_alarm_status(int n)
{
	alarmsig = 0;
}
int get_alarm_status()
{
	return alarmsig;
}

char *bg_process_status()
{
	return child_status;
}

void clear_bg_status()
{
	memset(child_status, '\0', 100);
}

void sig_int(__attribute__((unused)) int s)
{
	signaled=1;
	printf("\n");
	setsignals();
}

void proc_exit (__attribute__((unused)) int s)
{
	int status;
	if ( waitpid(childpid, &status, 1) == -1 )
	{
		perror("child pid wait fail");
	}
	else
	{
		sprintf(child_status, "background job completed %d", WEXITSTATUS(status));
		alarmsig = 1;
	}
}