#include	<stdio.h>
#include	<signal.h>
#include	<sys/wait.h>
#include    <errno.h>


int exitstatus(int n)
{
    switch(n){
        case EACCES:
            fprintf(stderr, "permission denied failed\n");
            return (126);
            break;
        case ENOENT:
            fprintf(stderr, "not found\n");
            return (127);
            break;
        default:
            return (n);
            break;
    }
}