#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<signal.h>
#include	<sys/wait.h>
#include	<string.h>
#include	<ctype.h>
#include    "splitline.h"
#include    "varlib.h"
#include    "flexstr.h"
#include    "process.h"
#include    "substitute.h"

char * substitute(char *args)
{
    int i;
    FLEXSTR newstring;
	fs_init(&newstring, 0);
	//int length = strlen(args)
    for (i=0; i < strlen(args); i++)
    {
        int start = 0;
        int end = 0;
        int y=0;

        if (args[i] == 92) {
            i++;
            fs_addch(&newstring, args[i]);
        }
        // get pid
        else if ((args[i] == '$') && (args[i+1] == '$'))
            svar_replace(&newstring, args[i+1], &i);
        // get exit status
        else if ((args[i] == '$') && (args[i+1] == '?'))
            svar_replace(&newstring, args[i+1], &i);

        // get actual variable
        else if ( args[i] == '$' )
        {
            i++;
            start = i;

            var_replace(args, &i, &y, &end);

            char *varstring, *newstr;
            varstring=emalloc(end-start);
            memset(varstring, '\0', (end-start)+1);
            strncpy(varstring, &args[start], (end-start));
            newstr=VLlookup( varstring );
            if (newstr == NULL)
                newstr = "";
            fs_addstr(&newstring, newstr);
            if ((y))
                fs_addch(&newstring, args[i]);
            free(varstring);

        }
        else
            fs_addch(&newstring, args[i]);
    }
    fs_addch(&newstring, '\0');
    return fs_getstr(&newstring);
}

void svar_replace(FLEXSTR *newstring, char flag, int *i)
{
    char datavar[5];
    if (flag == '$')
        sprintf(datavar, "%d", getpid());
    else if (flag == '?')
        sprintf(datavar, "%d", get_exit_status());
    fs_addstr(newstring, datavar);
    *i = *i + 1;
}

void var_replace(char *args, int *i, int *y, int *end)
{
    while (*i < strlen(args))
    {
        char c = args[*i];
        if (c == '$')
        {
            *end = *i;
            *i = *i - 1;
            break;
        }
        else if ( (c == '#') || (isdigit(c)) )
        {
            *end = *i+1;
            break;
        }
        else if (c == '\\')
        {
            *end = *i;
            *y = 1;
            *i = *i + 1;
            break;
        }
        else if (!(isalpha(c)) && (c != '_'))
        {
            *end = *i;
            *y = 1;
            break;
        }
        *i = *i+1;
        *end = *i;
    }
}