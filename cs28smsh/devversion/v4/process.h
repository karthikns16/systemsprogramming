#ifndef	PROCESS_H
#define	PROCESS_H


int process(char **args);
int do_command(char **args);
int execute(char **args);
int execute_cmd(char **args);
int get_exit_status();
void setsignals();


#endif
