#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<string.h>
#include	<signal.h>
#include	<sys/wait.h>
#include	"smsh.h"
#include	"splitline.h"
#include	"varlib.h"
#include	"process.h"
#include	"builtin.h"
#include	"substitute.h"
#include	"controlflow.h"
//#include	"flexstr.h"
/**
 **	small-shell version 5
 **		first really useful version after prompting shell
 **		this one parses the command line into strings
 **		uses fork, exec, wait, and ignores signals
 **
 **     hist: 2017-04-12: changed comment to say "version 5"
 **/

#define	DFL_PROMPT	"$ "
void setup();

int main(int argc, char *argv[])
{
	int start(char *prompt, FILE *fp);
	char *prompt;
	prompt = DFL_PROMPT ;
	setup();
	if (argc == 1)
		return start(prompt, stdin);
	else
	{
		int count = 0;
		int i = 1;
		char v[2];
		while ((argv[i] != NULL) && (count < 9))
		{
			sprintf(v, "%d", count);
			VLstore(v, argv[i]);
			i++;
			count++;
		}
		sprintf(v, "%d", i+1);
		VLstore("#",v);
		FILE *sfp;
		sfp = fopen(argv[1], "r");
		if (sfp == NULL)
		{
			fprintf(stderr, "%s: not found\n", argv[1]);
			exit(EXIT_FAILURE);
		}
		return start("", sfp);
	}
}

int start(char *prompt, FILE *fp)
{
	int	result=0;
	char *cmdline;
	char **arglist;

	while ( (cmdline = next_cmd(prompt, fp)) != NULL ){
		if (cmdline[0] != '#')
		{
			char *finalcmd;
			finalcmd=substitute(cmdline);
			if ( (arglist = splitline(finalcmd)) != NULL  ){
				result = process(arglist);
				freelist(arglist);
			}
			free(cmdline);
		}
	}
	if (get_if_state() != 0)
	{
		fatal("unexpected EOF", "error", 2);
	}
	return result;
}

void setup()
/*
 * purpose: initialize shell
 * returns: nothing. calls fatal() if trouble
 */
{
	extern char **environ;
	VLenviron2table(environ);
	setsignals();
}

void fatal(char *s1, char *s2, int n)
{
	fprintf(stderr,"Error: %s,%s\n", s1, s2);
	exit(n);
}