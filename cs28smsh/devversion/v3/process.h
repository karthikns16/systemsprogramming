#ifndef	PROCESS_H
#define	PROCESS_H


int process(char **args);
int do_command(char **args);
int execute(char **args);
int execute_cmd(char **args);
void setsignals();

//extern int exit_status;
#endif
