#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<signal.h>
#include	<sys/wait.h>
#include	<string.h>
#include	<ctype.h>
#include    "splitline.h"
#include    "varlib.h"
#include    "flexstr.h"
#include    "process.h"

char * substitute(char *args)
{
    int i;
    FLEXSTR newstring;
	fs_init(&newstring, 0);
    for (i=0; i < strlen(args); i++)
    {
        int start = 0;
        int end = 0;
        int y=0;
        if ((int) args[i] == 92)
        {
            i++;
            fs_addch(&newstring, args[i]);
        }
        else if ((args[i] == '$') && (args[i+1] == '$'))
        {
            char datavar[5];
            sprintf(datavar, "%d", getpid());
            fs_addstr(&newstring, datavar);
            i++;
        }
        else if ((args[i] == '$') && (args[i+1] == '?'))
        {
            char datavar[5];
			sprintf(datavar, "%d", get_exit_status());
			fs_addstr(&newstring, datavar);
			i++;
        }
        else if (( args[i] == '$' ))
        {
            i++;
            start = i;
            while (i < strlen(args))
            {
                char c = args[i];
                if (c == '$')
                {
                    end = i;
                    i--;
                    break;
                }
                else if (c == '#')
                {
                    end = i+1;
                    break;
                }
                else if (isdigit(c))
                {
                    end = i+1;
                    break;
                }
                else if (!(isalpha(c)) && (c != '_'))
                {
                    end = i;
                    y = 1;
                    break;
                }
                i++;
                end = i;
            }
            char *varstring;
            char *newstr;

            varstring=emalloc(end-start);
            memset(varstring, '\0', (end-start)+1);
            strncpy(varstring, &args[start], (end-start));
            newstr=VLlookup( varstring );
            if (newstr == NULL)
                newstr = "";
            fs_addstr(&newstring, newstr);
            if ((y))
                fs_addch(&newstring, args[i]);
            free(varstring);
        }
        else
        {
            fs_addch(&newstring, args[i]);
        }
    }
    fs_addch(&newstring, '\0');
    return fs_getstr(&newstring);
}

