#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<signal.h>
#include	<string.h>
#include	<sys/wait.h>
#include	<errno.h>
#include	"smsh.h"
#include	"builtin.h"
#include	"varlib.h"
#include	"controlflow.h"
#include	"process.h"
#include	"exitstatus.h"
#include	"handler.h"

/* process.c
 * command processing layer: handles layers of processing
 *
 * The process(char **arglist) function is called by the main loop
 * It sits in front of the do_command function which sits
 * in front of the execute() function.  This layer handles
 * two main classes of processing:
 *	a) process - checks for flow control (if, while, for ...)
 * 	b) do_command - does the command by
 *		         1. Is command built-in? (exit, set, read, cd, ...)
 *                       2. If not builtin, run the program (fork, exec...)
 *                    - also does variable substitution (should be earlier)
 */




int process(char *args[])
/*
 * purpose: process user command: this level handles flow control
 * returns: result of processing command
 *  errors: arise from subroutines, handled there
 */
{
	int		rv = 0;

	if ( args[0] == NULL )
		rv = 0;
	else if ( is_control_command(args[0]) )
		rv = do_control_command(args);
	else if ( ok_to_execute() )
		rv = do_command(args);
	set_exit_status(rv);
	return rv;
}

/*
 * do_command
 *   purpose: do a command - either builtin or external
 *   returns: result of the command
 *    errors: returned by the builtin command or from exec,fork,wait
 *
 */
int do_command(char **args)
{
	void varsub(char **);
	int  is_builtin(char **, int *);
	int  rv;

	if ( is_builtin(args, &rv) )
		return rv;
	rv = execute(args);
	return rv;
}

int execute(char *argv[])
/*
 * purpose: run a program passing it arguments
 * returns: status returned via wait, or -1 on error
 *  errors: -1 on fork() or wait() errors
 */
{

	extern char **environ;		/* note: declared in <unistd.h>	*/
	int	pid ;
	int	child_info = -1;
	int should_wait = 1;
	int argcount=0;
	while (argv[argcount] != '\0')
		argcount++;

	if ( argv[0] == NULL )		/* nothing succeeds		*/
		return 0;

	// logic to determine if this process has to be sent background
	if ( strcmp(argv[argcount-1], "&" ) == 0)
	{
		argv[argcount-1]='\0';
		should_wait = 0;
	}
	if ( (pid = fork())  == -1 )
		perror("fork");
	else if ( pid == 0 ){
		environ = VLtable2environ();
		setsignals();
		execvp(argv[0], argv);
		determine_status(errno);
		exit(get_exit_status());
	}
	else {
		// logic to handle the foreground child and background child
		if (should_wait)
		{
			if ( wait(&child_info) == -1 )
				perror("wait");
			else if (get_intr_status())
				return child_info + 128;
		}
		else
		{
			printf("%d\n", pid);
			set_child_pid(pid);
			signal (SIGCHLD, proc_exit);
		}
	}
	return WEXITSTATUS(child_info);
}

int execute_cmd(char *argv[])
/*
 * purpose: run a program withouf forking a child process and passing it arguments
 * returns: exit status of the last run command
 *  errors: none
 */
{
	extern char **environ;
	environ = VLtable2environ();
	setsignals();
	execvp(argv[1], &argv[1]);
	determine_status(errno);
	if (get_intr_status())
		return get_exit_status() + 128;
	exit(get_exit_status());
}

