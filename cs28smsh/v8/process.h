#ifndef	PROCESS_H
#define	PROCESS_H

/*
 * header for process.c package
 */

int process(char **args);
int do_command(char **args);
int execute(char **args);
int execute_cmd(char **args);
int get_exit_status();
void setsignals();
void set_exit_status(int n);
void sig_int(__attribute__((unused)) int s);
void proc_exit (__attribute__((unused)) int s);
char *bg_process_status();
int get_alarm_status();
void set_alarm_status(int n);
void clear_bg_status();

#endif
