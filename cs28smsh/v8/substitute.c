#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<signal.h>
#include	<sys/wait.h>
#include	<string.h>
#include	<ctype.h>
#include    "splitline.h"
#include    "varlib.h"
#include    "flexstr.h"
#include    "process.h"
#include    "substitute.h"
#include    "exitstatus.h"

/* substitute.c - parses the command and looks for variables
 *                replaces the variable with values
 *
 *    char * substitute(char *args) - main process that will handle substitution
 *    void svar_replace(FLEXSTR *newstring, char flag, int *i) - replaces special variables
 *                      such as $$, $?, $#, $0 .. $9 with its values
 *    void var_replace(char *args, int *i, int *y, int *end)  - replaces normal variables
 *                      with its values
 */



char * substitute(char *args)
/*
 * purpose: read next command line and process the variables
 * returns: dynamically allocated string holding command line
 *          with variable values replaced
 */
{
    int i, len=strlen(args);
    FLEXSTR newstring;
	fs_init(&newstring, 0);
    for (i=0; i < len; i++)
    {
        int start = 0;
        int end = 0;
        int y=0;

        // logic to handle "\$, \\ as prints the output as $ or \"
        if (args[i] == '\\') {
            i++;
            fs_addch(&newstring, args[i]);
        }
        // logic that handles $$
        else if ((args[i] == '$') && (args[i+1] == '$'))
            svar_replace(&newstring, args[i+1], &i);
        // logic that handles $?
        else if ((args[i] == '$') && (args[i+1] == '?'))
             svar_replace(&newstring, args[i+1], &i);
        // logic that handles $0 to $9
        else if ((args[i] == '$') && (args[i+1] >= '0' && args[i+1] <= '9' ))
             svar_replace(&newstring, args[i+1], &i);
        // logic that handles $#
        else if ((args[i] == '$') && (args[i+1] == '#'))
            svar_replace(&newstring, args[i+1], &i);

        // get actual variable
        else if ( args[i] == '$' )
        {
            i++;
            start = i;
            var_replace(args, &i, &y, &end);
            char *varstring, *newstr;
            varstring=emalloc(end-start);
            memset(varstring, '\0', (end-start)+1);
            strncpy(varstring, &args[start], (end-start));
            newstr=VLlookup( varstring );
            if (newstr == NULL)
                newstr = "";
            fs_addstr(&newstring, newstr);
            if (y)
                fs_addch(&newstring, args[i]);
            free(varstring);
        }
        else
            fs_addch(&newstring, args[i]);
    }
    fs_addch(&newstring, '\0');
    return fs_getstr(&newstring);
}

/*
 * purpose: process special variables
 * returns: dynamically allocated string holding command line
 *          with variable values replaced
 */

void svar_replace(FLEXSTR *newstring, char flag, int *i)
{
    char *datavar;
    // get pid
    if (flag == '$')
    {
        datavar=emalloc(5);
        sprintf(datavar, "%d", getpid());
    }
    // get exit status
    else if (flag == '?')
    {
        datavar=emalloc(5);
        sprintf(datavar, "%d", get_exit_status());
    }
    // get $0 to $9 and $# variables
    else if ((flag >= '0' && flag <= '9') || (flag ='#'))
    {
        char temp[] = {'\0', '\0'};
        temp[0] = flag;
        char *varout;
        varout  = VLlookup( temp );
        datavar=emalloc(strlen(varout));
        strcpy(datavar,varout);
    }
    fs_addstr(newstring, datavar);
    *i = *i + 1;
    free(datavar);

}

/*
 * purpose: process regular variables
 * returns: dynamically allocated string holding command line
 *          with variable values replaced
 */
void var_replace(char *args, int *i, int *y, int *end)
{
    int len = strlen(args);
    while (*i < len)
    {
        char c = args[*i];
        if (c == '$')
        {
            *end = *i;
            *i = *i - 1;
            break;
        }
        else if (c == '\\')
        {
            *end = *i;
            *y = 1;
            *i = *i + 1;
            break;
        }
        else if (!(isalnum(c)) && (c != '_'))
        {
            *end = *i;
            *y = 1;
            break;
        }
        *i = *i+1;
        *end = *i;
    }
}