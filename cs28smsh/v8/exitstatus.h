/*
 * header for exitstatus.c package
 */

void determine_status(int n);
int get_exit_status();
void set_exit_status(int n);