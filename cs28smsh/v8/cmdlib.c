#include	<stdio.h>
#include	<string.h>
#include	<ctype.h>
#include	<stdlib.h>
#include    <unistd.h>
#include    <limits.h>
#include    "builtin.h"
#include    "splitline.h"
#include	"flexstr.h"
#include    "varlib.h"
#include    "smsh.h"
#include    "process.h"
#include    "exitstatus.h"

/* cmdlib.c
 * contains the switch and the functions for builtin commands
 * namely, cd, read, exit, ., exec
 */


#define MAXCMD 6
// Structure that defines the builtin that are supported in this shell
struct cmds {
    char *cmd;
    int fval;
};

// method declarations
static int  find_item( char *name);
static void changedir(char *name);
static void readvar(char **name);
static void exitsh(char **name);
static void dot(char **name);
static void execcmd(char **name);

// intialization for the builtins which are supported
static struct cmds ctab[MAXCMD] =
{
    {"cd", 1},
    {"read", 2},
    {"exit", 3},
    {".", 4},
    {"exec", 5},
    {NULL, 0}
};


int CMDlookup( char **name )
/*
 * returns value of var or empty string if not there
 */
{
	int func;
    // switch to find which function to execute based on the builtin passed
    // by the user
	if ( (func = find_item(name[0]) ) != 0 ) {
        switch(func) {
            case 1:
                changedir(name[1]);
                break;
            case 2:
                readvar(name);
                break;
            case 3:
                exitsh(name);
                break;
            case 4:
                dot(name);
                break;
            case 5:
                execcmd(name);
                break;
            default:
                break;
        }
		return 1;
	}
	return 0;
}


static int find_item( char *name)
/*
 * searches table for an item
 * returns ptr to struct or NULL if not found
 * OR if (first_blank) then ptr to first blank one
 */
{
	int	i;
	char *s;

	if ( name == NULL )
		return 0;

	for( i = 0 ; i < MAXCMD  && ctab[i].cmd != NULL; i++ )
	{
		s = ctab[i].cmd;
		if ( strcmp(s,name) == 0 ){
			return ctab[i].fval;
		}
	}
	return 0;
}

static void changedir(char *name)
/*
 * implements the cd builtin
 * args: name of the directory to which shell to change to
 * if directory name is empty it will change to HOME dir
 * if HOME variable empty it will fail with error
 * rets: none
 */
{
    int rv = 1;
    char *temp;
    // cd to $HOME if name is NULL
    if (name == NULL)
        temp=VLlookup("HOME");
    else
        temp=name;
    // error if unable to change dir
    if ((rv = chdir(temp))!= 0)
    {
        set_exit_status(2);
        fprintf(stderr, "cd: can't cd to %s\n", temp);
    }
}

static void readvar(char **name)
/*
 * implements the read builtin
 * args: name of the variable to read value into
 * error: if variable name is empty or not a valid variable name
 * rets: none
 */
{
    // Fail if no variable name or invalid variable name
    if ((name[1] == NULL) || (!okname(name[1])))
    {
        fprintf(stderr, "read: %s: bad variable name\n", name[1]);
        set_exit_status(2);
        return;
    }
    char *value;
    int i = 0;
    char c;
    value=emalloc(sizeof(char)*1);
    if (value == NULL)
        return;
    // Logic to read the variable value from the user
    while ((c=getc(stdin)) != '\n')
    {
        value[i] = c;
        i++;
        value=erealloc(value, sizeof(char)*1);
        if (value == NULL)
            return;
    }
    value[i] = '\0';
    // Call VLStore to export the variable to environ
    VLstore(name[1], value);
    free(value);
}

static void exitsh(char **status)
/*
 * implements the exit builtin
 * args: exit command with arguments
 * error: if illegal number is passed to exit
 * rets: none
 */
{
    // exit the last exit status if argument to the exit command is empty
    if (!status[1])
        exit(get_exit_status());
    int i;
    int error = 0;
    // Fail if illegal character is passed as an argument to exit
    for (i = 0; i < strlen(status[1]); i++)
    {
        if (!isdigit(status[1][i]))
        {
            fprintf(stderr,"exit: Illegal number: %s\n", status[1]);
            error = 1;
            break;
        }
    }

    if (!error)
        exit(atoi(status[1]));
    // set the exit status if exit command fails
    set_exit_status(2);
}

static void dot(char **name)
/*
 * implements the . builtin
 * args: name of script file with arguments
 * error: if script file cannot be opened
 * rets: none
 */
{
    if (!name[1])
        return;
    FILE *dfp;
	dfp = fopen(name[1], "r");
	if (dfp == NULL)
	{
	    fprintf(stderr, "Can't open: %s\n", name[1]);
	    set_exit_status(2);
	}
	else
	    start("", dfp);
}

static void execcmd(char **name)
/*
 * implements the exec builtin
 * args: name of command to run exec on
 * rets: none
 */
{
    if (!name[1])
        return;
    execute_cmd(name);
}