#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<signal.h>
#include	<string.h>
#include	<sys/wait.h>
#include	<errno.h>
#include	"smsh.h"
#include	"builtin.h"
#include	"varlib.h"
#include	"controlflow.h"
#include	"process.h"
#include	"exitstatus.h"

/* handlder.c
 * contains the method that will save and report sigint, sigchld signals
 */

static int signaled = 0;
static int alarmsig = 0;
static char child_status[100];
static int childpid = 0;


/*
 * purpose: saves the child pid for a job that was send background
 * args: job id
 * returns: none
 */

void set_child_pid(int n)
{
	childpid = n;
}

/*
 * purpose: reports if a signaled SIG_INT was received
 * args: none
 * returns: 1 if SIG_INT was received 0 if not
 */

int get_intr_status()
{
	return signaled;
}

/*
 * purpose: saves the status if a signal SIG_CHLD was received
 * args: 1 or 0
 * returns: none
 */
void set_alarm_status(int n)
{
	alarmsig = n;
}

/*
 * purpose: reports if a SIG_CHLD was received
 * args: None
 * returns: 1 if SIG_CHLD was received 0 if not
 */

int get_alarm_status()
{
	return alarmsig;
}

/*
 * purpose: handler for SIG_INT
 * args: signal
 * returns: none
 */

void sig_int(__attribute__((unused)) int s)
{
	signaled=1;
	printf("\n");
	setsignals();
}


/*
 * purpose: handler for SIG_CHLD
 * args: signal
 * returns: none
 */

void proc_exit(__attribute__((unused)) int s)
{
	int status;
	if ( waitpid(childpid, &status, 1) == -1 )
	{
		perror("child pid wait fail");
	}
	else
	{
		sprintf(child_status, "background job completed %d", WEXITSTATUS(status));
		set_alarm_status(1);
	}
	childpid = 0;
}

/*
 * purpose: setup signal handlers and enables them
 * args: none
 * returns: none
 */

void setsignals()
{
	signal (SIGINT, sig_int);
	signal (SIGQUIT, SIG_DFL);
	signal (SIGALRM, SIG_DFL);
}

/*
 * purpose: reports the child_pid status
 * args: none
 * returns: verbose output of child_pid
 */

char *bg_process_status()
{
	return child_status;
}

/*
 * purpose: clears the child_status which verbose output of child_pid
 * args: none
 * returns: none
 */
void clear_bg_status()
{
	memset(child_status, '\0', 100);
}

