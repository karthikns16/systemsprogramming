#ifndef	CMDLIB_H
#define	CMDLIB_H

/*
 * header for cmdlib.c package
 */

void set_child_pid(int n);
int get_intr_status();
void set_alarm_status(int n);
int get_alarm_status();
void setsignals();
char *bg_process_status();
void clear_bg_status();


#endif