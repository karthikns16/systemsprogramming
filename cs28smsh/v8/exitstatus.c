#include	<stdio.h>
#include	<signal.h>
#include	<sys/wait.h>
#include    <errno.h>
#include    "exitstatus.h"

/* exitstatus.c
 * contains the method that will save and report last exit status
 */

static int exit_status = 0;

/*
 * purpose: determine the status errno when execvp fails
 * args: errno from the execvp
 * returns: none
 * details: sets the exit status variable based on errno
 */
void determine_status(int n)
{
    switch(n){
        case EACCES:
            fprintf(stderr, "permission denied failed\n");
            set_exit_status(126);
            break;
        case ENOENT:
            fprintf(stderr, "not found\n");
            set_exit_status(127);
            break;
        default:
            set_exit_status(n);
            break;
    }
}

/*
 * purpose: reports the last exit status
 * args: none
 * returns: exit status
 */
int get_exit_status()
{
	return exit_status;
}

/*
 * purpose: saves the last exit status
 * args: exit status from the last command executed
 * returns: none
 */
void set_exit_status(int n)
{
	exit_status=n;
}
