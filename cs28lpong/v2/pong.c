#include <stdio.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <curses.h>
#include <signal.h>
#include <string.h>

#include "globalvars.h"
#include "alarmlib.h"
#include "paddle.h"
#include "ball.h"
#include "handler.h"

/*
 *	pong.c - main program that implements the pong
 *		functions
 *			main -- starts the program, calls other methods required for pong
 *			wrap_up -- ends the curses window and exits the program SUCCESS
 *			get_term_size -- determines the terminal size and captues the
 *							 center of screen
 *			game_timer -- tracks the elapsed time of the game
 *						  updates the screen with the time
 *			game_over -- implements the game over screen
 *			is_game_playable -- quits if the size of the terminal is too small
 *								to show the game screen
 *			draw_border -- draws the border wall for the game
 */

// Method declarations

static void draw_border(char pattern, int start,  char axis, int howmuch);
static void wrap_up(int g);
static void set_up();
static int get_term_size();
static void is_game_playable();


int main()
/*
* Main program that initializes game variable
* Receives paddle signals and passes paddle.c
*/
{
	/* Variable to capture paddle movement*/
	char c;

	/* random number generator */
	srand(getpid());

    get_term_size();
    set_up();
	// Setup the alarm
	serve( 1000 / TICKS_PER_SEC );	/* send millisecs per tick */

    while ( balls_left > 0 && ( c = getch() ) != QUIT )
    {
		if ( alrm > 0 )
			move_ball();
		if ( c == UP )
			paddle_up();
		else if ( c == DOWN )
			paddle_down();
	}
	wrap_up(ENDGAME);
}

static void wrap_up(int g)
/*
*	wraps up the program
*	args: ENDGAME or GAMEOVER as g
*	rets: NONE
*/
{
	serve( ENDGAME );
	if (g == ENDGAME)
	{
		mvprintw(the_screen.ycenter + NXT_OR_PRV,
			the_screen.xcenter - 10,
			"Press any key to quit");
	}
	getch();
	endwin();		/* put back to normal	*/
	exit(EXIT_SUCCESS);
}

static int get_term_size()
/*
*	Gets the current terminal size
*	Determines the center of the screen
*	args: none
*	rets: 1 on SUCCESS
*/
{
	// Variables to store the terminal size
	struct winsize w;
	int rv;
	rv = ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	if ( rv != 0 )
	{
		perror("Unable to get winsize");
		exit(EXIT_FAILURE);
	}

	// Set the rows/cols of the terminal
	the_screen.rows = w.ws_row;
	the_screen.cols = w.ws_col;

	// Determines and set the center of the terminal
	the_screen.xcenter = (PADDING+the_screen.cols)/2;
	the_screen.ycenter = (PADDING+the_screen.rows)/2;

	is_game_playable();

	// Set the right and bottom edge of the screen to control paddle movement
	BOT_ROW = the_screen.rows - PADDING - NXT_OR_PRV;
	RIGHT_EDGE = the_screen.cols - PADDING - NXT_OR_PRV;
	return rv;
}


static void set_up()
/*
*	Initializes the game screen
*	args: none
*	rets: none
*/
{
    initscr();		/* turn on curses	*/
	noecho();		/* turn off echo	*/
	cbreak();		/* turn off buffering	*/
	reset_position();
	enable_signals();

    // Draws the border of the screen
    draw_border('|', PADDING, 'y', the_screen.rows - PADDING);
    draw_border('-', PADDING, 'x', the_screen.cols - PADDING);
    draw_border('-', the_screen.rows - PADDING,  'x',
    			the_screen.cols - PADDING);

	// Initializes paddle
	paddle_init();

    // Writes the top banner of the game
    sprintf(str_balls_left,"%d", balls_left);
    mvprintw(PADDING - NXT_OR_PRV, PADDING, "BALLS LEFT:");
    mvprintw(PADDING - NXT_OR_PRV, 15, str_balls_left);
    mvprintw(PADDING - NXT_OR_PRV, (the_screen.cols - PADDING - 25),
    		"TOTAL TIME:");
	refresh();

}


void game_timer()
/*
*	calculates the game time
*	updates the elapsed time in the screen
*	args: none
*	rets: none
*/
{
	// Variable to elapsed timer
	char elapsed_time[5], strtemp[2];
	int temp = timer/1000;
	memset(elapsed_time, '\0', 5*sizeof(char));
	memset(elapsed_time, '\0', 2*sizeof(char));
	elapsed_time[2]=':';

	// String operations to update elapsed time to screen
	sprintf(strtemp, "%02d", temp/60);
	strcat(elapsed_time, strtemp);
	strcat(elapsed_time, ":");
	sprintf(strtemp, "%02d", temp%60);
	strcat(elapsed_time, strtemp);

	// Update the string to the screen
	mvprintw(PADDING - NXT_OR_PRV, RIGHT_EDGE - 10, elapsed_time);
}

void game_over()
/*
*	Game over screen
*	args: none
*	rets: none
*/
{
	mvprintw(the_screen.ycenter, the_screen.xcenter - 10,
			"Game Over!!!");
	mvprintw(the_screen.ycenter + NXT_OR_PRV,
			the_screen.xcenter - 10,
			"Press any key to quit");
	wrap_up(GAMEOVER);
}


static void is_game_playable()
/*
*	Method used to check if the game area is too small to play
*	args: none
*	rets: none
*/
{
	// Exits if the screen size is smaller to fit all the information
	if (the_screen.rows < MINROWS || the_screen.cols < MINCOLS)
	{
		fprintf(stderr, "the pong screen is too small."
						"increase screen size to %d*%d"
						"screen to continue playing",
				MINCOLS, MINROWS);
		exit(EXIT_FAILURE);
	}

}

static void draw_border(char pattern, int start, char axis, int howmuch)
/*
*	Method used to draw borders for the given specification
*	args: pattern, start of border, axis and length of borders
*	rets: none
*/
{
    int i = PADDING;
    while ( i < howmuch)
    {
        if (axis == 'y')
            mvaddch(i, start, pattern);
        else if (axis == 'x')
            mvaddch(start, i, pattern);
        i++;
    }
}
