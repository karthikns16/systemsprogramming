#include <stdio.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <curses.h>
#include <signal.h>
#include "ball.h"
#include "globalvars.h"

/*
 *	paddle.c - initiates paddle, implements the paddle movement capabilities
 *		functions
 *			paddle_init -- intiates the paddle and put it position
 *			paddle_up -- moves the paddle UP by one unit when 'k' is pressed
 *			paddle_down -- moves the paddle DOWN by one unit when 'm' is pressed
 *			paddle_contact -- check if ball made contact with the paddle
 */


// paddle struct
struct pppaddle
{
    int pad_top, pad_bot, pad_col;
    char pad_char;
};

// initializes it as static
static struct pppaddle paddle;


void paddle_init()
/*
*	Initializes the paddle
*	args: none
*	rets: none
*/
{
    int i = PADDING + NXT_OR_PRV;
    paddle.pad_top = i;
    paddle.pad_col = RIGHT_EDGE;
    paddle.pad_char = PDL_SYMBOL;
    // Sets the paddle size to 1/3rd of size of terminal
    paddle.pad_bot = (int) ((BOT_ROW + NXT_OR_PRV) / PADDLE_SIZE) + i;
    while (i < paddle.pad_bot)
    {
        mvaddch(i, paddle.pad_col, paddle.pad_char);
        i++;
    }
}

void paddle_up()
/*
*	Moves the paddle UP
*	args: none
*	rets: none
*/
{
    if (paddle.pad_top != PADDING + NXT_OR_PRV)
    {
        // add "#" to the top position
        paddle.pad_top--;
        mvaddch(paddle.pad_top, paddle.pad_col, paddle.pad_char);

        // add blank to the bottom position
        paddle.pad_bot--;
        mvaddch(paddle.pad_bot, paddle.pad_col, BLANK);
    }
    move(LINES-1, COLS-1);
    bounce_or_lose();
}

void paddle_down()
/*
*	Moves the paddle DOWN
*	args: none
*	rets: none
*/
{
    if (paddle.pad_bot != BOT_ROW + NXT_OR_PRV)
    {
        // add blank to the top position
        mvaddch(paddle.pad_top, paddle.pad_col, BLANK);
        paddle.pad_top++;

        // add "#" to the bottom of the paddle
        mvaddch(paddle.pad_bot, paddle.pad_col, paddle.pad_char);
        paddle.pad_bot++;
    }
    move(LINES-1, COLS-1);
    bounce_or_lose();
}

int paddle_contact(int y,int x)
/*
*	determines if a contact was made at the right edges
*	args: y, x position of ball in screen
*	rets: 0 if contact was made, -1 if not
*/
{
    if ( (x == paddle.pad_col) &&
         ((y >= paddle.pad_top) && (y <= paddle.pad_bot) ) )
    {
        return 0;
    }
    return -1;
}
