#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <signal.h>
#include "globalvars.h"

/*
 *	handler.c - implements the signal handler and enables signals
 *		functions
 *			update_alarm	-- handler for SIGALRM, increments the alarm counter
 *			confirm_quit	-- handler for SIGINT, quits pong if 'y' is key'ed in
 *			enable_signals	-- setup the SIGALRM and SIGINT signals for the pong
 */


void update_alarm(__attribute__((unused)) int s)
/*
*	SIGALRM handler: handler in case SIGALRM is received
*	args: signal
*	rets: none
*/
{
	// Updates the timer
	timer = timer + 20;
	// Increments the alarm counter
	alrm++;
}

void confirm_quit(__attribute__((unused)) int s)
/*
*	SIGINT handler: prompt user and exit if user confirms quit
*	args: none
*	rets: none
*/
{
	int	ans;
	static int count = 0;

	standout();				/* prompt user	*/
	mvprintw(the_screen.ycenter, the_screen.xcenter,
			" [%2d] Quit (y/n)? ",
			++count);
	standend();

	while(TRUE){
		ans = getch();			/* get answer	*/
		if ( ans == 'y' ){
			endwin();
			exit(EXIT_SUCCESS);
		}
		if ( ans == 'n' )
			break;
	}
	move(the_screen.ycenter, the_screen.xcenter - 10);
	clrtoeol();
	refresh();
}

void enable_signals()
/*
*	setup the signals and its handler
*	args: signal
*	rets: none
*/
{
	// variable to hold sigint, sigalrm signals
	struct sigaction alrm_act, quit_act;
	// enable sigint
	quit_act.sa_handler=confirm_quit;
	sigfillset(&quit_act.sa_mask);
	sigaction( SIGINT, &quit_act, NULL );
	// enable sigalrm
	alrm_act.sa_handler=update_alarm;
	sigemptyset( &alrm_act.sa_mask );
	alrm_act.sa_flags = 0;
	sigaction( SIGALRM, &alrm_act, NULL );
}