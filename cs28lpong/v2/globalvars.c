#include "pong.h"
#include "globalvars.h"

/*
 *	globalvars.c - defines the global variables for the pong program
 */

// Variable to hold the screen size
struct windtl the_screen;

// Variable for timer and to track alarms
int timer = 0;
int alrm = 0;

// Boundary for paddle, ball
int TOP_ROW = PADDING + NXT_OR_PRV;
int LEFT_EDGE = PADDING + NXT_OR_PRV;
int BOT_ROW = 0;
int RIGHT_EDGE = 0;

// To track the number of balls left
int balls_left = BALLS;
char str_balls_left[2] = {'\0','\0'};


