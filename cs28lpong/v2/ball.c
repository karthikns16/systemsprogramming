#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <string.h>
#include "pong.h"
#include "paddle.h"
#include "globalvars.h"

/*
 *	ball.c - implements the logic needed to move the ball in the screen
 *		functions
 *			update_ball_position -- move the ball new position in the screen
 *			reset_position -- resets the position of the ball to the center of the
 *							  screen
 *			move_ball -- prepares the the_ball variable which will be used to
 *						 move the ball through update_ball_position
 *			bounce_or_lose -- check if ball bounces when it walls/paddle
 *			check_for_paddle_contact -- handles screen changes when the ball hits
 *										paddle or handles the numbers of balls left of game
 */




// Method declaration
static void update_ball_position(int x_cur, int y_cur);
static int check_for_paddle_contact();
int bounce_or_lose();

// Structure to track the pong ball
struct ppball {
    int	x_pos;
    int x_dir;
    int y_pos;
    int y_dir;
    int y_delay;
    int y_count;
    int x_delay;
    int x_count;
    int rows;
    int cols;
    char symbol;
};

// Initializes the ball as static
static struct ppball the_ball;

void reset_position()
/*
*	Reset the position of the ball at the start of game
*	and when the ball goes out of court
*	args: none
*	rets: none
*/
{
	the_ball.y_pos = the_screen.ycenter;
	the_ball.x_pos = the_screen.xcenter;
	the_ball.y_count = the_ball.y_delay = RANDOM;
	the_ball.x_count = the_ball.x_delay = RANDOM;
	the_ball.y_dir = NXT_OR_PRV  ;
	the_ball.x_dir = NXT_OR_PRV  ;
	the_ball.symbol = DFL_SYMBOL ;
	mvaddch(the_ball.y_pos, the_ball.x_pos, the_ball.symbol);
}

void move_ball()
/*
*	Readies the ball be moved to new position
*	args: none
*   rets: none
*/
{
	alrm--;				/* reduce the alarm counter */
	if ((timer%1000) == 0)
		game_timer();
	int	y_cur, x_cur, moved;
	y_cur = the_ball.y_pos ;		/* old spot		*/
	x_cur = the_ball.x_pos ;
	moved = 0 ;

	if ( the_ball.y_delay > 0 && --the_ball.y_count == 0 ){
		the_ball.y_pos += the_ball.y_dir ;	/* move	*/
		the_ball.y_count = the_ball.y_delay  ;	/* reset*/
		moved = 1;
	}

	if ( the_ball.x_delay > 0 && --the_ball.x_count == 0 ){
		the_ball.x_pos += the_ball.x_dir ;	/* move	*/
		the_ball.x_count = the_ball.x_delay  ;	/* reset*/
		moved = 1;
	}
	if ( moved )
		update_ball_position(x_cur, y_cur);
}

static void update_ball_position(int x_cur, int y_cur)
/*
*	Updates the position of ball in hte screen
*	args: x co-ordinate, y co-ordinate to which ball
*		  has to be moved
*	rets: none
*/
{
	int bounce = bounce_or_lose();
	// Logic to handle ball bouncing of the edges
	if (!bounce)
	{
		//move the ball next position if it not a bounce
		mvaddch(y_cur, x_cur, BLANK);
		mvaddch(the_ball.y_pos, the_ball.x_pos, the_ball.symbol);
	}
	else
	{
		//if bounce then put the ball back to position before the bounce
		the_ball.y_pos = y_cur;		/* old spot		*/
		the_ball.x_pos = x_cur;
		mvaddch(y_cur, x_cur, BLANK);
	}
	move(LINES-1, COLS-1);		/* park cursor	*/
	refresh();
}

int bounce_or_lose()
/* bounce_or_lose: if ball hits walls, change its direction
 *   args: address to ppball
 *   rets: 1 if a bounce happened, 0 if not
 *	 rets: -1 if the ball moves out of the court
 */
{

	int	return_val = 0 ;
	// logic to handle when the ball bounces of the edges
	if ( the_ball.y_pos == TOP_ROW )
		the_ball.y_dir = NXT_OR_PRV , return_val = BOUNCE ;
	else if ( the_ball.y_pos == BOT_ROW )
		the_ball.y_dir = -NXT_OR_PRV , return_val = BOUNCE;
	if ( the_ball.x_pos == LEFT_EDGE )
		the_ball.x_dir = NXT_OR_PRV , return_val = BOUNCE ;
	else if ( the_ball.x_pos == RIGHT_EDGE)
		return_val = check_for_paddle_contact();
	return return_val;
}

static int check_for_paddle_contact()
{
	// if there no contact with paddle on hte right edge
	// go the IF block and handle balls left
	int return_val = 0;
	if (paddle_contact(the_ball.y_pos, the_ball.x_pos) != 0)
	{
		balls_left--;
		// reset the position of ball if moved out of play
		reset_position();
		sprintf(str_balls_left,"%d", balls_left);
		mvprintw(PADDING - NXT_OR_PRV, 15, str_balls_left);
		// if no balls left game over
		if (balls_left == 0)
		{
			game_over();
			return_val = LOSE;
		}
	}
	// if there is contact with paddle in the right edge
	// update the speed and direction of the ball
	else
	{
		the_ball.x_delay=RANDOM;
		the_ball.y_delay=RANDOM;
		the_ball.x_dir = -NXT_OR_PRV;
		return_val = NOBOUNCE;
	}
	return return_val;
}