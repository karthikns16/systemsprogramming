// Used in setup
#define RANDOM (rand() % (MAXDELAY + 1 - MINDELAY) + MINDELAY)


// Used in bouce or lose
#define MAXDELAY 10
#define MINDELAY 5

// Defines the edges of the pong court
#define PADDING         3
#define	DFL_SYMBOL	    'O'
#define	BLANK		    ' '
#define	TICKS_PER_SEC   50
#define NXT_OR_PRV      1
#define BALLS           3
#define QUIT            'Q'
#define UP              'k'
#define DOWN            'm'
#define MINROWS         16
#define MINCOLS         50
#define BOUNCE          0
#define NOBOUNCE        1
#define LOSE            -1
#define GAMEOVER        10
#define ENDGAME         0
#define PADDLE_SIZE     3
#define PDL_SYMBOL      '#'


// Boundary for paddle.c, ball.c
extern int TOP_ROW;
extern int LEFT_EDGE;
extern int BOT_ROW;
extern int RIGHT_EDGE;

// extern variable used in ball.c and pong.c
extern int alrm;
extern int timer;
extern int balls_left;
extern char str_balls_left[2];

// Structure to track the terminal size and center of terminal
struct windtl {
  int rows;
  int cols;
  int xcenter;
  int ycenter;
};

extern struct windtl the_screen;