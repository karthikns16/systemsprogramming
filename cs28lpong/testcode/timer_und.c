#include	<sys/time.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <curses.h>
#include <signal.h>
void printsomething(int s);
int set_ticker( int n_msecs );
int main()
{



	signal(SIGALRM, printsomething);
	initscr();

	set_ticker(20);
	printf("alarm not yet \n");
	getch();
	endwin();

	return 0;
}

void printsomething(int s)
{
	signal(SIGALRM, SIG_IGN);
	printf("alarm!!!!\t");
	signal(SIGALRM, printsomething);
}

int set_ticker( int n_msecs )
/*
 *	arg in milliseconds, converted into micro seoncds
 *	Returns -1 on error, 0 if no error
 */
{

	struct itimerval new_timeset, old_timeset;
	long	n_sec, n_usecs;

	n_sec = n_msecs / 1000 ;
	n_usecs = ( n_msecs % 1000 ) * 1000L ;

	new_timeset.it_interval.tv_sec  = n_sec;	/* set reload  */
	new_timeset.it_interval.tv_usec = n_usecs;	/* new ticker value */
	new_timeset.it_value.tv_sec     = n_sec  ;	/* store this	*/
	new_timeset.it_value.tv_usec    = n_usecs ;	/* and this 	*/



	if ( setitimer( ITIMER_REAL, &new_timeset, &old_timeset ) != 0 ){
		printf("Error with timer..errno=%d\n", errno );
		return -1;
	}

	return 0;
}