#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#define MAXNUM 10
#define MINNUM 1

int
main(int argc, char *argv[])
{
   int j, r, nloops;
   unsigned int seed;

//   if (argc != 3) {
//       fprintf(stderr, "Usage: %s <seed> <nloops>\n", argv[0]);
//       exit(EXIT_FAILURE);
//   }
    printf("pid %d\n", getpid());
    seed = getpid();
    nloops = 5;
    srand(seed);
    //r = (rand() % (max + 1 - min)) + min
    for (j = 0; j < nloops; j++) {
       //r =  (rand() % (MAXNUM + 1 - MINNUM) + MINNUM);
       r = rand() % MAXNUM;
       printf("%d\n", r);
    }

    exit(EXIT_SUCCESS);
}