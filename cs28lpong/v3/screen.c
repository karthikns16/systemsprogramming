#include <stdio.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <curses.h>
#include <signal.h>
#include <string.h>
#include "screen.h"
#include "constants.h"

/*
 *	screen.c - main program that implements the pong
 *		functions
 *			init_screen -- initializes the screen
 *			get_term_size -- determines the terminal size and captues the
 *							 center of screen
 *			is_game_playable -- quits if the size of the terminal is too small
 *								to show the game screen
 *			draw_border -- draws the border wall for the game
 *			int get_xcenter -- return x co-ordinate of screen center
 *			int get_ycenter -- return y co-ordinate of screen center
 *			int get_rightedge -- return rightedge of screen
 *			int get_leftedge  -- return leftedge of screen
 *			int get_botrow -- return bottom row of screen
 *			int get_toprow -- return top row of screen
 */

static int get_term_size();
static void is_game_playable();
static void draw_border(char pattern, int start, char axis, int howmuch);

struct windtl {
  int rows;
  int cols;
  int xcenter;
  int ycenter;
  int TOP_ROW;
  int LEFT_EDGE;
  int BOT_ROW;
  int RIGHT_EDGE;
};


static struct windtl the_screen;


void init_screen()
/*
*	initializes the screen
*	args: none
*	rets: none
*/
{
    // Boundary for paddle, ball
	the_screen.TOP_ROW = PADDING + NXT_OR_PRV;
	the_screen.LEFT_EDGE = PADDING + NXT_OR_PRV;
	the_screen.BOT_ROW = 0;
	the_screen.RIGHT_EDGE = 0;

    get_term_size();
    initscr();		/* turn on curses	*/
	noecho();		/* turn off echo	*/
	cbreak();		/* turn off buffering	*/

	// Draws the border of the screen
    draw_border('|', PADDING, 'y', the_screen.rows - PADDING);
    draw_border('-', PADDING, 'x', the_screen.cols - PADDING);
    draw_border('-', the_screen.rows - PADDING,  'x',
    			the_screen.cols - PADDING);
    // Writes the top banner of the game
    mvprintw(PADDING - NXT_OR_PRV, (the_screen.cols - PADDING - 25),
    		"TOTAL TIME:");
	refresh();
}

static int get_term_size()
/*
*	Gets the current terminal size
*	Determines the center of the screen
*	args: none
*	rets: 1 on SUCCESS
*/
{
	// Variables to store the terminal size
	struct winsize w;
	int rv;
	rv = ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	if ( rv != 0 )
	{
		perror("Unable to get winsize");
		exit(EXIT_FAILURE);
	}

	// Set the rows/cols of the terminal
	the_screen.rows = w.ws_row;
	the_screen.cols = w.ws_col;

	// Determines and set the center of the terminal
	the_screen.xcenter = (PADDING+the_screen.cols)/2;
	the_screen.ycenter = (PADDING+the_screen.rows)/2;

	is_game_playable();

	// Set the right and bottom edge of the screen to control paddle movement
	the_screen.BOT_ROW = the_screen.rows - PADDING - NXT_OR_PRV;
	the_screen.RIGHT_EDGE = the_screen.cols - PADDING - NXT_OR_PRV;
	return rv;
}


static void is_game_playable()
/*
*	Method used to check if the game area is too small to play
*	args: none
*	rets: none
*/
{
	// Exits if the screen size is smaller to fit all the information
	if (the_screen.rows < MINROWS || the_screen.cols < MINCOLS)
	{
		fprintf(stderr, "the pong screen is too small."
						"increase screen size to %d*%d"
						" size to continue playing\n",
				MINCOLS, MINROWS);
		exit(EXIT_FAILURE);
	}

}

static void draw_border(char pattern, int start, char axis, int howmuch)
/*
*	Method used to draw borders for the given specification
*	args: pattern, start of border, axis and length of borders
*	rets: none
*/
{
    int i = PADDING;
    while ( i < howmuch)
    {
        if (axis == 'y')
            mvaddch(i, start, pattern);
        else if (axis == 'x')
            mvaddch(start, i, pattern);
        i++;
    }
}

int get_xcenter()
/*
*	retrieves return x co-ordinate of screen center
*	args: none
*	rets: xcenter
*/
{
	return the_screen.xcenter;
}

int get_ycenter()
/*
*	retrieves return y co-ordinate of screen center
*	args: none
*	rets: ycenter
*/
{
	return the_screen.ycenter;
}

int get_rightedge()
/*
*	retrieves return rightedge of screen
*	args: none
*	rets: RIGHT_EDGE
*/
{
	return the_screen.RIGHT_EDGE;
}
int get_leftedge()
/*
*	retrieves return leftedge of screen
*	args: none
*	rets: LEFT_EDGE
*/
{
	return the_screen.LEFT_EDGE;
}

int get_botrow()
/*
*	retrieves return bottom row of screen
*	args: none
*	rets: BOT_ROW
*/
{
	return the_screen.BOT_ROW;
}

int get_toprow()
/*
*	retrieves return top row of screen
*	args: none
*	rets: TOP_ROW
*/
{
	return the_screen.TOP_ROW;
}