/*
    header file for paddle.c
*/


void paddle_init();

void paddle_up();

void paddle_down();

int paddle_contact(int y,int x);