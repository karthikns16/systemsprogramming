/*
    header file for screen.c
*/

void init_screen();
int get_xcenter();
int get_ycenter();
int get_rightedge();
int get_leftedge();
int get_botrow();
int get_toprow();