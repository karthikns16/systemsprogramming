#include <stdio.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <curses.h>
#include <signal.h>
#include <string.h>

#include "constants.h"
#include "alarmlib.h"
#include "paddle.h"
#include "ball.h"
#include "handler.h"
#include "screen.h"

/*
 *	pong.c - main program that implements the pong
 *		functions
 *			main -- starts the program, calls other methods required for pong
 *			setup -- call the init of paddle, ball, screen, handler and alarm
 *			wrap_up -- ends the curses window and exits the program SUCCESS
 *			game_timer -- tracks the elapsed time of the game
 *						  updates the screen with the time
 *			game_over -- implements the game over screen
 */

// Method declarations


static void wrap_up(int g);
static void set_up();



int main()
/*
* Main program that initializes game variable
* Receives paddle signals and passes paddle.c
*/
{
	/* Variable to capture paddle movement*/
	char c;

	/* random number generator */
	srand(getpid());

    set_up();
	// Setup the alarm
	serve( 1000 / TICKS_PER_SEC );	/* send millisecs per tick */

    while ( isball_available() && ( c = getch() ) != QUIT )
    {
		if ( getalrm() > 0 )
			move_ball();
		if ( c == UP )
			paddle_up();
		else if ( c == DOWN )
			paddle_down();
	}
	wrap_up(ENDGAME);
}

static void wrap_up(int g)
/*
*	wraps up the program
*	args: ENDGAME or GAMEOVER as g
*	rets: NONE
*/
{
	serve( ENDGAME );
	if (g == ENDGAME)
	{
		mvprintw(get_ycenter() + NXT_OR_PRV,
			get_xcenter() - 10,
			"Press any key to quit");
	}
	getch();
	endwin();		/* put back to normal	*/
	exit(EXIT_SUCCESS);
}



static void set_up()
/*
*	Initializes the game screen
*	args: none
*	rets: none
*/
{

	init_screen();
	ball_init();
	enable_signals();
	paddle_init();
}


void game_timer()
/*
*	calculates the game time
*	updates the elapsed time in the screen
*	args: none
*	rets: none
*/
{
	// Variable to elapsed timer
	char elapsed_time[5], strtemp[2];
	int temp = gettimer()/1000;
	memset(elapsed_time, '\0', 5*sizeof(char));
	memset(elapsed_time, '\0', 2*sizeof(char));
	elapsed_time[2]=':';

	// String operations to update elapsed time to screen
	sprintf(strtemp, "%02d", temp/60);
	strcat(elapsed_time, strtemp);
	strcat(elapsed_time, ":");
	sprintf(strtemp, "%02d", temp%60);
	strcat(elapsed_time, strtemp);

	// Update the string to the screen
	mvprintw(PADDING - NXT_OR_PRV, get_rightedge() - 10, elapsed_time);
}

void game_over()
/*
*	Game over screen
*	args: none
*	rets: none
*/
{
	mvprintw(get_ycenter(), get_xcenter() - 10,
			"Game Over!!!");
	mvprintw(get_ycenter() + NXT_OR_PRV,
			get_xcenter() - 10,
			"Press any key to quit");
	wrap_up(GAMEOVER);
}




