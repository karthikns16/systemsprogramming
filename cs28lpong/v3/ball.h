/*
    header file for ball.c
*/

void ball_init();
int isball_available();
void move_ball();
int bounce_or_lose();