#include <stdio.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <curses.h>
#include <signal.h>
#include "pong.h"

// Paddle struct
struct pppaddle
{
    int pad_top, pad_bot, pad_col;
    char pad_char;
};

// Initializes it as static
static struct pppaddle paddle;


void paddle_init()
/*
*	Initializes the paddle
*	args: none
*	rets: none
*/
{
    int i = PADDING + NXT_OR_PRV;
    paddle.pad_top = i;
    paddle.pad_col = RIGHT_EDGE;
    paddle.pad_char = PDL_SYMBOL;
    // Sets the paddle size to 1/3rd of size of terminal
    paddle.pad_bot = (int) ((BOT_ROW + NXT_OR_PRV) / PADDLE_SIZE) + i;
    while (i < paddle.pad_bot)
    {
        mvaddch(i, paddle.pad_col, paddle.pad_char);
        i++;
    }
}

void paddle_up()
/*
*	Moves the paddle UP
*	args: none
*	rets: none
*/
{
    if (paddle.pad_top != PADDING + NXT_OR_PRV)
    {
        paddle.pad_top--;
        mvaddch(paddle.pad_top, paddle.pad_col, paddle.pad_char);

        paddle.pad_bot--;
        mvaddch(paddle.pad_bot, paddle.pad_col, BLANK);
    }
    move(LINES-1, COLS-1);
}

void paddle_down()
/*
*	Moves the paddle DOWN
*	args: none
*	rets: none
*/
{
    if (paddle.pad_bot != BOT_ROW + NXT_OR_PRV)
    {
        mvaddch(paddle.pad_top, paddle.pad_col, BLANK);
        paddle.pad_top++;

        mvaddch(paddle.pad_bot, paddle.pad_col, paddle.pad_char);
        paddle.pad_bot++;
    }
    move(LINES-1, COLS-1);
}

int paddle_contact(int y,int x)
/*
*	determines if a contact was made at the right edges
*	args: y, x position of ball in screen
*	rets: 0 if contact was made, -1 if not
*/
{
    if ( (x == paddle.pad_col) &&
         ((y >= paddle.pad_top) && (y <= paddle.pad_bot) ) )
    {
        return 0;
    }
    return -1;
}
