/*
    Header file and defines for pong.c
*/
int bounce_or_lose();

// Used in setup
#define	X_DELAY	    5
#define	Y_DELAY		10


// Used in bouce or lose
#define MAXDELAY 10
#define MINDELAY 5

// Defines the edges of the pong court
#define PADDING         3
#define	DFL_SYMBOL	    'O'
#define	BLANK		    ' '
#define	TICKS_PER_SEC   50
#define NXT_OR_PRV      1
#define PDL_SYMBOL      '#'
#define PADDLE_SIZE     3
#define BALLS           1
#define QUIT            'Q'
#define UP              'k'
#define DOWN            'm'
#define MINROWS         16
#define MINCOLS         50
#define BOUNCE          0
#define NOBOUNCE        1
#define LOSE            -1
#define GAMEOVER        10
#define ENDGAME         0

// Boundary for paddle, ball
extern int TOP_ROW;
extern int LEFT_EDGE;
extern int BOT_ROW;
extern int RIGHT_EDGE;

// Structure to track the pong ball
struct ppball {
    int	x_pos;
    int x_dir;
    int y_pos;
    int y_dir;
    int y_delay;
    int y_count;
    int x_delay;
    int x_count;
    int rows;
    int cols;
    char symbol;
};

// Structure to track the terminal size and center of terminal
struct windtl {
  int rows;
  int cols;
  int xcenter;
  int ycenter;
};