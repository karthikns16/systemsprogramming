#include <stdio.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <curses.h>
#include <signal.h>
#include <string.h>
#include "pong.h"
#include "alarmlib.h"
#include "paddle.h"


// Method declarations
int get_term_size();
void reset_position();
void set_up();
void draw_border(char pattern, int start,  char axis, int howmuch);
void draw_banner();
int bounce_or_lose(struct ppball *bp);
void game_over();
void wrap_up();

// Global variables
static struct ppball the_ball;
static struct windtl the_screen;
static int timer;


int main()
/*
* Main program that initializes game variable
* Receives paddle signals and passes paddle.c
*/
{
	/* Intializing game variables*/
	char c;
	TOP_ROW = PADDING + NXT_OR_PRV;
	LEFT_EDGE = PADDING + NXT_OR_PRV;
	BOT_ROW = 0;
	RIGHT_EDGE = 0;

	/* track the balls left */
	balls_left = BALLS;
	str_balls_left[1]='\0';

	/* random number generator */
	srand(getpid());

    get_term_size();
    set_up();

    while ( balls_left > 0 && ( c = getch() ) != QUIT )
    {
		if ( c == UP )
			paddle_up();
		else if ( c == DOWN )
			paddle_down();
	}
	wrap_up();
}

void wrap_up()
/*
*	wraps up the program
*/
{
	set_ticker( 0 );
	endwin();		/* put back to normal	*/
}

int get_term_size()
/*
*	Gets the current terminal size
*	Determines the center of the screen
*	args: none
*	rets: 1 on SUCCESS
*/
{
	// Variables to store the terminal size
	struct winsize w;
	int rv;

	rv = ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	if ( rv != 0 )
	{
		perror("Unable to get winsize");
		exit(EXIT_FAILURE);
	}

	// Set the rows/cols of the terminal
	the_screen.rows = w.ws_row;
	the_screen.cols = w.ws_col;

	// Determines and set the center of the terminal
	the_screen.xcenter = (PADDING+the_screen.cols)/2;
	the_screen.ycenter = (PADDING+the_screen.rows)/2;

	// Exits if the screen size is smaller to fit all the information
	if (the_screen.rows < MINROWS || the_screen.cols < MINCOLS)
	{
		fprintf(stderr, "the pong screen is too small."
			"increase screen size to %d*%d screen to continue playing",
			MINCOLS, MINROWS);
		exit(EXIT_FAILURE);
	}

	// Set the right and bottom edge of the screen to control paddle movement
	BOT_ROW = the_screen.rows - PADDING - NXT_OR_PRV;
	RIGHT_EDGE = the_screen.cols - PADDING - NXT_OR_PRV;
	return rv;
}
void reset_position()
/*
*	Reset the position of the ball at the start of game
*	and when the ball goes out of court
*	args: none
*	rets: none
*/
{
	the_ball.y_pos = the_screen.ycenter;
	the_ball.x_pos = the_screen.xcenter;
	the_ball.y_count = the_ball.y_delay = Y_DELAY ;
	the_ball.x_count = the_ball.x_delay = X_DELAY ;
	the_ball.y_dir = NXT_OR_PRV  ;
	the_ball.x_dir = NXT_OR_PRV  ;
	the_ball.symbol = DFL_SYMBOL ;
	mvaddch(the_ball.y_pos, the_ball.x_pos, the_ball.symbol);
}
void set_up()
/*
*	Initializes the game screen
*	args: none
*	rets: none
*/
{
	// Method declarations
    void	ball_move(int);
    void	confirm_quit(int);
    initscr();		/* turn on curses	*/
	noecho();		/* turn off echo	*/
	cbreak();		/* turn off buffering	*/

    signal (SIGINT, confirm_quit);

	reset_position();

    // Draws the border of the screen
    draw_border('|', PADDING, 'y', the_screen.rows - PADDING);
    draw_border('-', PADDING, 'x', the_screen.cols - PADDING);
    draw_border('-', the_screen.rows - PADDING,  'x',
    			the_screen.cols - PADDING);
	// Initializes paddle
	paddle_init();
    // Writes the top banner of the game
    mvprintw(PADDING - NXT_OR_PRV, PADDING, "BALLS LEFT:");
    sprintf(str_balls_left,"%d", balls_left);
    mvprintw(PADDING - NXT_OR_PRV, 15, str_balls_left);
    mvprintw(PADDING - NXT_OR_PRV, (the_screen.cols - PADDING - 25),
    		"TOTAL TIME:");

	refresh();
	signal( SIGALRM, ball_move );
	// Setup the alarm
	set_ticker( 1000 / TICKS_PER_SEC );	/* send millisecs per tick */
}


void draw_border(char pattern, int start, char axis, int howmuch)
/*
*	Method used to draw borders for the given specification
*	args: pattern, start of border, axis and length of borders
*	rets: none
*/
{
    int i = PADDING;
    while ( i < howmuch)
    {
        if (axis == 'y')
            mvaddch(i, start, pattern);
        else if (axis == 'x')
            mvaddch(start, i, pattern);
        i++;
    }
}


void ball_move(int s)
/*
*	Moves the ball when the alarm goes off
*	args: signal, rets: none
*/
{
	// Method declaration for game timer
	void game_timer();
	// Logic to update elapsed time of the game
	timer = timer + 20;
	if ((timer%1000) == 0)
		game_timer();
	int	y_cur, x_cur, moved;
	signal( SIGALRM , SIG_IGN );		/* dont get caught now 	*/
	y_cur = the_ball.y_pos ;		/* old spot		*/
	x_cur = the_ball.x_pos ;		/* old spot		*/
	moved = 0 ;

	if ( the_ball.y_delay > 0 && --the_ball.y_count == 0 ){
		the_ball.y_pos += the_ball.y_dir ;	/* move	*/
		the_ball.y_count = the_ball.y_delay  ;	/* reset*/
		moved = 1;
	}

	if ( the_ball.x_delay > 0 && --the_ball.x_count == 0 ){
		the_ball.x_pos += the_ball.x_dir ;	/* move	*/
		the_ball.x_count = the_ball.x_delay  ;	/* reset*/
		moved = 1;
	}
	if ( moved ){
		int bounce = bounce_or_lose( &the_ball );
		// Logic to handle ball bouncing of the edges
		if (!bounce)
		{
			mvaddch(y_cur, x_cur, BLANK);
			mvaddch(the_ball.y_pos, the_ball.x_pos, the_ball.symbol);
		}
		else
		{
			the_ball.y_pos = y_cur;		/* old spot		*/
			the_ball.x_pos = x_cur;		/* old spot		*/
			mvaddch(y_cur, x_cur, BLANK);
		}
		move(LINES-1, COLS-1);		/* park cursor	*/
		refresh();
	}
	signal(SIGALRM, ball_move);		/* re-enable handler	*/
}

void game_timer()
/*
*	calculates the game time
*	updates the elapsed time in the screen
*	args: none
*	rets: none
*/
{
	char elapsed_time[5], strtemp[2];
	int temp = timer/1000;
	memset(elapsed_time, '\0', 5*sizeof(char));
	memset(elapsed_time, '\0', 2*sizeof(char));
	elapsed_time[2]=':';

	// String operations
	sprintf(strtemp, "%02d", temp/60);
	strcat(elapsed_time, strtemp);
	strcat(elapsed_time, ":");
	sprintf(strtemp, "%02d", temp%60);
	strcat(elapsed_time, strtemp);

	// Update the string to the screen
	mvprintw(PADDING - NXT_OR_PRV, RIGHT_EDGE - 10, elapsed_time);
}


int bounce_or_lose(struct ppball *bp)
/* bounce_or_lose: if ball hits walls, change its direction
 *   args: address to ppball
 *   rets: 1 if a bounce happened, 0 if not
 *	 rets: -1 if the ball moves out of the court
 */
{

	int	return_val = 0 ;

	if ( bp->y_pos == TOP_ROW )
		bp->y_dir = NXT_OR_PRV , return_val = BOUNCE ;
	else if ( bp->y_pos == BOT_ROW )
		bp->y_dir = -NXT_OR_PRV , return_val = BOUNCE;

	if ( bp->x_pos == LEFT_EDGE )
		bp->x_dir = NXT_OR_PRV , return_val = BOUNCE ;

	// Logic to handle bounce off of the paddle
	else if ( bp->x_pos == RIGHT_EDGE)
	{
		// if there no contact with paddle on hte right edge
		// go the IF block and handle balls left
		if (paddle_contact(bp->y_pos, bp->x_pos) != 0)
		{
			balls_left--;
			reset_position();
			sprintf(str_balls_left,"%d", balls_left);
			mvprintw(PADDING - NXT_OR_PRV, 15, str_balls_left);
			// if no balls left game over
			if (balls_left == 0)
			{
				game_over();
				return_val = LOSE;
				return return_val;
			}
		}
		// if there is contact with paddle in the right edge
		// update the speed and direction of the ball
		else
		{
			bp->x_delay=(rand() %
						(MAXDELAY + 1 - MINDELAY) + MINDELAY);
			bp->y_delay=(rand() %
						(MAXDELAY + 1 - MINDELAY) + MINDELAY);
			bp->x_dir = -NXT_OR_PRV , return_val = NOBOUNCE;
		}
	}
	return return_val;
}

void game_over()
/*
*	Game over screen
*	args: none
*	rets: none
*/
{
	mvprintw(the_screen.ycenter, the_screen.xcenter,
			"Game Over!!!");
	mvprintw(the_screen.ycenter + NXT_OR_PRV,
			the_screen.xcenter,
			"Press any key to quit");
	wrap_up();
}

void confirm_quit(int s)
/*
*	SIGINT handler -- prompt user and exit if user confirms quit
*	args: none
*	rets: none
*/
{
	int	ans;
	static int count = 0;

	standout();				/* prompt user	*/
	mvprintw(the_screen.ycenter, the_screen.xcenter,
			" [%2d] Quit (y/n)? ",
			++count);
	standend();

	while(TRUE){
		ans = getch();			/* get answer	*/
		if ( ans == 'y' ){
			endwin();
			exit(EXIT_SUCCESS);
		}
		if ( ans == 'n' )
			break;
	}
	move(the_screen.ycenter, the_screen.xcenter);
	clrtoeol();
	refresh();
}