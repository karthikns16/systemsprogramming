/*
    Header file and defines for pong.c
*/


// Used in setup
#define	X_DELAY	    5
#define	Y_DELAY		10


// Used in bouce or lose
#define MAXDELAY 10
#define MINDELAY 5

// Defines the edges of the pong court
#define PADDING         3
#define	DFL_SYMBOL	    'O'
#define	BLANK		    ' '
#define	TICKS_PER_SEC   75
#define NXT_OR_PRV      1
#define PDL_SYMBOL      '#'
#define PADDLE_SIZE     3
#define BALLS           20
#define QUIT            'Q'
#define UP              'k'
#define DOWN            'm'
#define MINROWS         16
#define MINCOLS         50
#define BOUNCE          0
#define NOBOUNCE        1
#define LOSE            -1

// Boundary for paddle, ball
// variable definition in a header file is not advised and I will do extern
int TOP_ROW;
int LEFT_EDGE;
int BOT_ROW;
int RIGHT_EDGE;

// To track the number of balls left
int balls_left;
char str_balls_left[2];

// Structure to track the pong ball
struct ppball {
    int	x_pos;
    int x_dir;
    int y_pos;
    int y_dir;
    int y_delay;
    int y_count;
    int x_delay;
    int x_count;
    int rows;
    int cols;
    char symbol;
};

// Structure to track the terminal size and center of terminal
struct windtl {
  int rows;
  int cols;
  int xcenter;
  int ycenter;
};