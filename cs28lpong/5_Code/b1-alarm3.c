#include	<stdio.h>
#include	<curses.h>
#include	<signal.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	"alarmlib.h"

/*
 * bounce1-sig3.c -- move a char back and forth across the screen
 *     uses: curses for graphics, set_ticker() for time
 *     note: on Ctrl-C, confirm user wants to quit
 *     new!: allows user to control speed with 's' and 'f'
 */

#define	BLANK	' '
#define	BALL	'O'
#define	ROW	10
#define	LEFT	10
#define	RIGHT	40
#define	DELAY	200		/* milliseconds		*/

static int	col, dir;	/* state of ball	*/
static	int	delay = DELAY;

int main(int ac, char **av)
{
	void	confirm_quit(int);	/* Ctrl-C handler	*/
	void	mover(int);		/* sigalrm handler	*/
	void	handle_key(int);

	if ( ac > 1 )
		delay = atoi(av[1]);	/* user specfied delay	*/
	col = LEFT;
	dir = +1;

	initscr();			/* set up curses	*/
	signal(SIGINT, confirm_quit);	/* set up ^C handler	*/

	move(ROW, col);			/* draw ball		*/
	addch(BALL);
	refresh();

	signal(SIGALRM, mover);		/* set up alarm handler	*/
	set_ticker(delay);		/* start timer		*/

	while(1){
		int key;

		/* display status and get key */
		mvprintw(0,0,"   (%2d,%2d) %2d", ROW, col, dir);
		refresh();
		key = getch();		/* block on user input	*/
		handle_key(key);
	}
	endwin();
	return 0;
}

/* process input: SPACE => bounce, s => slower, f => faster	*/
void
handle_key(int k)
{
	if ( k == ' ' ){
		dir = -dir;
		return;
	}
	if ( k == 's' )
		delay = delay * 2;
	else if ( k == 'f' )
		delay = 1 + delay / 2;	/* prevent going to 0	*/
	set_ticker(delay);
}

/* SIGALRM handler -- move ball one unit in current dir, bounce	*/
void
mover(int s)
{
	mvaddch(ROW, col, BLANK);		/* erase	*/
	col += dir;				/* move		*/
	mvaddch(ROW, col, BALL);		/* draw 	*/
	move(0, 0);				/* park cursor	*/
	refresh();				/* show it	*/

	if ( col == RIGHT && dir == +1 )	/* bounce?	*/
		dir = -1;
	else if ( col == LEFT && dir == -1 )
		dir = +1;
}

/* SIGINT handler -- ask user to confirm quit			*/
void
confirm_quit(int s)
{
	int	ans;
	static int count = 0;

	standout();					/* prompt user	*/
	mvprintw(ROW+2, LEFT, " [%2d] Quit (y/n)? ", ++count);
	standend();

	while(1){
		ans = getch();				/* get answer	*/
		if ( ans == 'y' ){
			endwin();
			exit(0);
		}
		if ( ans == 'n' )
			break;
	}
	move(ROW+2, LEFT);
	clrtoeol();
	refresh();
}
