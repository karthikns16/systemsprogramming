#include	<stdio.h>
#include	<signal.h>
#include	<unistd.h>

/*
 * sleep1.c
 *		purpose	show how sleep works
 *		usage	sleep1
 *		info	sets handler, sets alarm, pauses, then returns
 */

// #define	SHHHH

int main()
{
	void	onbell(int);
	int	retval;

	printf("about to sleep for 4 seconds\n");
	signal(SIGALRM, onbell);			/* catch it	*/
	alarm(4);					/* set clock	*/
	retval = pause();				/* do nothing	*/
	printf("pause returned %d\n", retval);
	perror("pause says");
	printf("Morning so soon?\n");			/* back to work	*/
	return 0;
}

void
onbell(int s)
{
#ifndef SHHHH
	printf("Alarm received from kernel\n");
#endif
}
