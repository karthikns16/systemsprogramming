#include	<stdio.h>
#include	<curses.h>
#include	<signal.h>
#include	<stdlib.h>
#include	<unistd.h>

/*
 * bounce1-sig1.c -- move a char back and forth across the screen
 *     uses: curses for graphics, NEW: alarm and pause for time 
 *     note: on Ctrl-C, confirm user wants to quit
 */

#define	BLANK	' '
#define	BALL	'O'
#define	ROW	10
#define	LEFT	10
#define	RIGHT	20

static int	col, dir;	/* state of ball	*/
				/* have to be global	*/
int main()
{
	void	confirm_quit(int);	/* Ctrl-C handler	*/
	void	mover(int);		/* sigalrm handler	*/

	col = LEFT;
	dir = +1;

	initscr();			/* set up curses	*/
	signal(SIGINT, confirm_quit);	/* set up ^C handler	*/

	move(ROW, col);			/* draw ball		*/
	addch(BALL);
	refresh();

	signal(SIGALRM, mover);		/* set up alarm handler	*/
	alarm(1);			/* set alarm for 1 sec	*/

	while(1){
		// mover();
		// sleep(1);
		pause();		/* wait for a signal	*/
	}
	endwin();
	return 0;
}

/* move ball one unit in current direction; bounce at walls	*/
void
mover(int s)
{
	mvaddch(ROW, col, BLANK);		/* erase	*/
	col += dir;				/* move		*/
	mvaddch(ROW, col, BALL);		/* draw 	*/
	refresh();				/* show it	*/

	if ( col == RIGHT && dir == +1 )	/* bounce?	*/
		dir = -1;
	else if ( col == LEFT && dir == -1 )
		dir = +1;

	alarm(1);				/* reset timer	*/
}

/* SIGINT handler -- prompt user and exit if user confirms quit	*/
void
confirm_quit(int s)
{
	int	ans;
	static int count = 0;

	standout();				/* prompt user	*/
	mvprintw(ROW+2, LEFT, " [%2d] Quit (y/n)? ", ++count);
	standend();

	while(1){
		ans = getch();			/* get answer	*/
		if ( ans == 'y' ){
			endwin();
			exit(0);
		}
		if ( ans == 'n' )
			break;
	}
	move(ROW+2, LEFT);
	clrtoeol();
	refresh();
}
