#include	<stdio.h>
#include	<signal.h>
#include	<unistd.h>

/*
 * sleep1.c
 *		purpose	show how sleep works
 *		usage	sleep1
 *		info	sets handler, sets alarm, pauses, then returns
 */

// #define	SHHHH

int main()
{
	void	onbell(int);
	void	on_sigint(int);
	int	retval;

	printf("about to sleep for 4 seconds\n");
	signal(SIGALRM, onbell);			/* catch it	*/
	signal(SIGINT,  on_sigint);
	alarm(4);					/* set clock	*/
	retval = pause();				/* do nothing	*/
	printf("pause returned %d\n", retval);
	perror("pause says");
	printf("Morning so soon?\n");			/* back to work	*/
	return 0;
}

void
onbell(int s)
{
#ifndef SHHHH
	printf("Alarm received from kernel\n");
#endif
}

// called on sigint
// prints the signal number passed to it and then returns
//
void on_sigint(int s)
{
	printf("just got signal %d\n", s);
	printf("returning to current activity...\n");
}

