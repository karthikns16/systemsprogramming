#include	<stdio.h>
#include	<curses.h>
#include	<signal.h>
#include	<stdlib.h>
#include	<unistd.h>

/*
 * bounce1-sleep.c -- move a char back and forth across the screen
 *     uses: curses for graphics, sleep for time control
 */

#define	BLANK	' '
#define	BALL	'O'
#define	ROW	10
#define	LEFT	10
#define	RIGHT	20

int	col, dir;	/* state of ball, global for a reason	*/

int main()
{
	void	mover();

	col = LEFT;			/* initialize ball	*/
	dir = +1;
	initscr();			/* set up curses	*/

	move(ROW, col);			/* draw ball		*/
	addch(BALL);
	refresh();

	while(1){
		mover();		/* move it		*/
		usleep(100000);		/* wait a sec	*/
	}
	endwin();
	return 0;
}

/* move ball one unit in current direction.  Bounce at boundaries	*/
void
mover()
{
	mvaddch(ROW, col, BLANK);		/* erase	*/
	col += dir;				/* move		*/
	mvaddch(ROW, col, BALL);		/* draw 	*/
	refresh();				/* show it	*/

	if ( col == RIGHT && dir == +1 )	/* bounce?	*/
		dir = -1;
	else if ( col == LEFT && dir == -1 )
		dir = +1;
}
