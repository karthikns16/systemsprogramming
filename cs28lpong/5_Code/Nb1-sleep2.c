#include	<curses.h>
#include	<signal.h>
#include	<stdlib.h>
#include	<unistd.h>

/*
 * bounce1-sleep2.c -- move a char back and forth across the screen
 *     uses: curses for graphics, sleep for time control
 *      new! on Ctrl-C, confirm user wants to quit
 */

#define	BLANK	' '
#define	BALL	'O'
#define	ROW	10
#define	LEFT	10
#define	RIGHT	20

int	col, dir;		/* state of ball	*/

int
main()
{
	void	confirm_quit(int);	/* Ctrl-C handler	*/
	void	mover();		/* animator		*/
	int	moves = 0;
	void	(*prevhandler)(int);


	col = LEFT;
	dir = +1;

	initscr();			/* set up curses	*/
	prevhandler = signal(SIGINT, confirm_quit);	/* set up ^C handler	*/

	move(ROW, col);			/* draw ball		*/
	addch(BALL);
	refresh();

	while(1){
		mover();
		sleep(1);				/* wait a sec	*/
		moves++;
		if ( moves == 3 )
			signal(SIGINT, prevhandler);
	}
	endwin();
	return 0;
}

/*
 * mover - move ball one screen cell in current direction
 *   call: by main at regular intervals
 */
void
mover()
{
	mvaddch(ROW, col, BLANK);		/* erase	*/
	col += dir;				/* move		*/
	mvaddch(ROW, col, BALL);		/* draw 	*/
	move(LINES-1, 0);			/* park cursor	*/
	refresh();				/* show it	*/

	if ( col == RIGHT && dir == +1 )	/* bounce?	*/
		dir = -1;
	else if ( col == LEFT && dir == -1 )
		dir = +1;
}

/*
 * confirm_quit - confirm that user wants to end the game
 *   call: as an signal handler when user presses Ctrl-C
 *   todo: prevent multiple signals?
 */
void
confirm_quit(int s)
{
	int	ans;
	static int count = 0;

	standout();					/* prompt user	*/
	mvprintw(ROW+2, LEFT, " [%2d] Quit (y/n)? ", ++count);
	standend();

	while(1){
		ans = getch();				/* get answer	*/
		if ( ans == 'y' ){
			endwin();
			exit(0);
		}
		if ( ans == 'n' )
			break;
	}
	move(ROW+2, LEFT);
	clrtoeol();
	refresh();
}
